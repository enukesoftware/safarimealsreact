package com.example.tooglebutton;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Switch;

public class mySwitch extends Switch {
    public mySwitch(Context context) {
        super(context);
    }

    public mySwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public mySwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    


}
