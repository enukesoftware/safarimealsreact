/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar, SafeAreaView, AsyncStorage, DeviceEventEmitter } from 'react-native';
import { createStackNavigator, createAppContainer, createSwitchNavigator, createDrawerNavigator } from "react-navigation";
import { Constants, Images } from './src/utils'
import SplashScreen from 'react-native-splash-screen'
import { strings } from './src/utils/strings'
import NetworkService from './src/services/NetworkService'
import { Provider } from 'react-redux'
import store from './src/redux/store'
import MyComponent from './src/components/MyComponent'
import Tutorial from './src/components/tutorial'
import NavigationService from './src/services/NavigationServices'
import SelectLocality from './src/components/select-locality'
import SearchAddress from './src/components/search-address'
import Geocoder from 'react-native-geocoding'
import AddressAutocomplete from './src/components/address-autocomplete'
import Dashboard from './src/components/dashbaord'
import RestaurantDetails from './src/components/restaurant-details'
import Filter from './src/components/filter'
import Basket from './src/components/basket'
import Login from './src/components/login'
import Order from './src/components/order'
import Orders from './src/components/orders'
// import AsyncStorage from '@react-native-community/async-storage';
import RestaurantMenu from './src/components/restaurant-menu'
import AddAddress from './src/components/add-address'
import Register from './src/components/register'
import ChangePassword from './src/components/change-password'
import Profile from './src/components/profile'
import ForgetPassword from './src/components/forget-password'
import Checkout from './src/components/checkout'
import SelectDeliveryAddress from './src/components/select-delivery-address'
import TableBooking from './src/components/table-booking'
import CustomNavigationDrawer from './src/components/custom/custom-navigation-drawer'
import CuisinesSelection from './src/components/cuisines-selection'
import TableBookingHistory from './src/components/table-booking-history'
import OrderConfirmation from './src/components/order-confirmation'
import Deals from './src/components/deals'
import ViewOrder from './src/components/view-order'
import ModifyBasket from './src/components/modify-basket'
import RestaurantMenuModify from './src/components/restaurant-menu-modify'


import ContactUs from './src/components/contact-us'
import AboutUs from './src/components/about-us'
import AddOns from './src/components/add-ons'

import AddOnsModify from './src/components/add-ons-modify'


import TrackDriver from './src/components/track-driver'
import firebase from 'react-native-firebase'
import FirebaseDatabase from './src/firebase-database'
import CountrySelection from './src/components/country-selection'


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

// let isVisitedSteps = false;
export default class App extends Component {

  static navigationOptions = ({ navigation }) => ({
    header: null
  });

  state = {
    isVisitedSteps: false,
    isLogin: false,
  }

  componentWillUnmount() {

  }

  componentWillMount() {
    const iosConfig = {
      clientId: '601376659172-t3vp6s7ekgr0vdi455cn8aas7v42ekm3.apps.googleusercontent.com',
      appId: '1:601376659172:ios:84ee40c2885cec4f',
      apiKey: 'AIzaSyB9S0eb5X1IwDLT4QwvSV4YupNAwjVDVUQ',
      databaseURL: 'https://taxiyefood.firebaseio.com',
      storageBucket: 'taxiyefood.appspot.com',
      messagingSenderId: '601376659172',
      projectId: 'taxiyefood',
      // enable persistence by adding the below flag
      persistence: true,
    }

    const androidConfig = {
      clientId: '601376659172-b83ma460mm0tqp68q74aquocckqkkjl8.apps.googleusercontent.com',
      appId: '1:601376659172:android:84ee40c2885cec4f',
      apiKey: 'AIzaSyBicog1eiMvajOaiJJk4gulrRR4ccN3GSE',
      databaseURL: 'https://taxiyefood.firebaseio.com',
      storageBucket: 'taxiyefood.appspot.com',
      messagingSenderId: '601376659172',
      projectId: 'taxiyefood',
      // enable persistence by adding the below flag
      persistence: true,
    }
    firebase.initializeApp(
      Platform.OS === 'ios' ? iosConfig : androidConfig,
      'taxiyefood'
    );

    AsyncStorage.getItem(Constants.STORAGE_KEY.isVisitedSteps, (error, result) => {
      if (error) {
        console.log("ERROR:" + JSON.stringify(error))
      }
      else {
        // this.setState({ userData: JSON.parse(result) })
        // console.warn(JSON.parse(result));
        if (result) {
          console.log("RESULT:" + result)
          if (result === 'true') {
            // isVisitedSteps = true
            this.setState({
              isVisitedSteps: true
            })
          } else {
            // isVisitedSteps = false
            this.setState({
              isVisitedSteps: false
            })
          }
        }
      }
    })

    //get last stored login status information
    AsyncStorage.getItem(Constants.STORAGE_KEY.isLogin, (error, result) => {
      if (error) {
        console.log("ERROR:" + JSON.stringify(error))
      }
      else {
        if (result) {
          this.setState({
            isLogin: true,
          }, () => {
            console.log("LOGIN_STATE_UPDATED")
          })
        }
      }
    })

  }


  componentDidMount() {
    console.log("APP_DID_MOUNT")
    SplashScreen.hide()
    Geocoder.setApiKey(Constants.GOOGLE_API_KEY)
    this.checkPermission();
    this.createNotificationListeners();


  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    console.log("FETCH_TOKEN")
    let fcmToken = await AsyncStorage.getItem(Constants.STORAGE_KEY.deviceToken);
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // alert("TOKEN:"+fcmToken)
        // user has a device token
        console.log('fcmToken:', fcmToken);
        await AsyncStorage.setItem(Constants.STORAGE_KEY.deviceToken, fcmToken);
      }
    } else {
      // alert("TOKEN:"+fcmToken)
    }
    console.log('fcmToken:', fcmToken);
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  showNotification(data) {
    console.log("ShowingNotification:"+JSON.stringify(data))
    const channel = new firebase.notifications.Android.Channel('TaxiyeFood', 'TaxiyeFood', firebase.notifications.Android.Importance.Max)
    firebase.notifications().android.createChannel(channel);
    const notification = new firebase.notifications.Notification()
      .setNotificationId(data.notificationID)
      .setTitle(data.title)
      .setBody(data.body)
      .android.setBigText(data.body)
      .setSound("default")
      .android.setAutoCancel(true)
      .setData(data)
      .android.setChannelId('TaxiyeFood')

    firebase.notifications().displayNotification(notification).then(() => console.log("Show")).catch(err => console.error("error " + err));
  }

  async createNotificationListeners() {

    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((res) => {
      console.log("NOTI_onNotification:", res)
      // console.log(JSON.stringify(notification))
      AsyncStorage.getItem(Constants.STORAGE_KEY.isLogin, (error, result) => {
        if (!error && result) {
          if (res && res.data) {
            let data = res.data;
            if (data.type) {
              if (data.type == 'customer' || data.type == 'driver') {
                console.log("received initial notification " + JSON.stringify(data));
                DeviceEventEmitter.emit(Constants.EVENTS.orderNotification)
              }
            }
            this.showNotification(data)
          }
        }
      })
    });



    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((res) => {
        AsyncStorage.getItem(Constants.STORAGE_KEY.isLogin, (error, result) => {
          if (!error && result) {
            if (res && res.notification) {
              console.log("RES_FOUND1")
              let notification = res.notification;
              console.log("received initial notification " + JSON.stringify(notification.data));
              if (notification.data && notification.data.type && (notification.data.type == 'customer' || notification.data.type == 'driver')) {
                NavigationService.navigate('Orders')
              }
            } else {
              console.log("RES_NOT_FOUND1")
            }
          }
        })
      });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */

    firebase
      .notifications()
      .getInitialNotification()
      .then(res => {
        console.log("NOTI_getInitialNotification:")
        if (res) {
        }
      });

    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      console.log("NOTI_onMessage:")
      console.log(JSON.stringify(message))
      //process data message
      // console.log('NOTIFICATION4');
      // console.log("JSON.stringify:", JSON.stringify(message));
    });



  }



  renderNavigator() {
    if (this.state.isVisitedSteps) {
      return (
        <LocalityNavigator />
      )
    } else {
      return (
        <TutorialNavigator />
      )
    }
  }

  render() {
    return (
      <Provider store={store}>

        <NetworkService />
        {/* <FirebaseDatabase/> */}
        <StatusBar
          barStyle="light-content"
          backgroundColor={Constants.color.primaryDark} />
        <View style={styles.containerStyle}>
          {this.renderNavigator()}
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Constants.color.primaryDark,
  },
  containerStyle: {
    flex: 1,
    width: '100%',
    backgroundColor: '#ffffff'
  },
});

const DashboardDrawerNavigator = createDrawerNavigator({
  DashboardContainer: {
    screen: Dashboard,
  },
}, {
  navigationOptions: ({ navigation }) => {
    return {
      header: null,
    };
  },
  contentComponent: CustomNavigationDrawer

});

const DashboardDrawerContainer = createAppContainer(DashboardDrawerNavigator);

const DashboardStack = createStackNavigator({
  SelectLocality: {
    screen: SelectLocality
  },
  SearchAddress: {
    screen: SearchAddress
  },
  AddressAutocomplete: {
    screen: AddressAutocomplete
  },
  RestaurantMenu: {
    screen: RestaurantMenu
  },
  RestaurantDetails: {
    screen: RestaurantDetails
  },
  Filter: {
    screen: Filter
  },
  Basket: {
    screen: Basket
  },
  Order: {
    screen: Order
  },
  Orders: {
    screen: Orders
  },
  AddAddress: {
    screen: AddAddress
  },
  Register: {
    screen: Register
  },
  ChangePassword: {
    screen: ChangePassword
  },
  Profile: {
    screen: Profile
  },
  Login: {
    screen: Login
  },
  ForgetPassword: {
    screen: ForgetPassword
  },
  Checkout: {
    screen: Checkout
  },
  SelectDeliveryAddress: {
    screen: SelectDeliveryAddress
  },
  TableBooking: {
    screen: TableBooking
  },
  Dashboard: {
    screen: DashboardDrawerNavigator
  },
  CuisinesSelection: {
    screen: CuisinesSelection
  },
  TableBookingHistory: {
    screen: TableBookingHistory
  },
  OrderConfirmation: {
    screen: OrderConfirmation
  },
  Deals: {
    screen: Deals
  },
  ContactUs: {
    screen: ContactUs
  },
  AboutUs: {
    screen: AboutUs
  },
  Deals: {
    screen: Deals
  },
  ViewOrder: {
    screen: ViewOrder
  },
  AddOns: {
    screen: AddOns
  },

  ModifyBasket: {
    screen: ModifyBasket
  },
  RestaurantMenuModify: {
    screen: RestaurantMenuModify
  },
  AddOnsModify: {
    screen: AddOnsModify
  },
  TrackDriver: {
    screen: TrackDriver

  },
  CountrySelection: {
    screen: CountrySelection

  }

},

  {
    initialRouteName: 'Dashboard',
  });

const TutorialStack = createStackNavigator({
  Tutorial: {
    screen: Tutorial
  },
  RestaurantMenu: {
    screen: RestaurantMenu
  },
},
  {
    initialRouteName: 'Tutorial',
  })

const OrderStack = createStackNavigator({
  OrdersPage: {
    screen: Orders
  },
},
  {
    initialRouteName: 'OrdersPage',
  })

const LocalityStack = createStackNavigator({
  Locality: {
    screen: SelectLocality
  },
  SearchAddress: {
    screen: SearchAddress
  },
  AddressAutocomplete: {
    screen: AddressAutocomplete
  },
  Orders: {
    screen: Orders
  },
},
  {
    initialRouteName: 'Locality',
  })


const TutorialStackContainer = createAppContainer(
  createSwitchNavigator(
    {
      TutorialStack: TutorialStack,
      DashboardStack: DashboardStack,
      LocalityStack: LocalityStack,
      // OrderStack:OrderStack
    },
    {
      initialRouteName: "TutorialStack"
    }
  )
);



const LocalityStackContainer = createAppContainer(
  createSwitchNavigator(
    {
      LocalityStack: LocalityStack,
      DashboardStack: DashboardStack,
      // OrderStack:OrderStack
    },
    {
      initialRouteName: "LocalityStack"
    }
  )
);

// const AppContainer = createAppContainer(AppStackNavigator);
const TutorialNavigator = () => (
  <TutorialStackContainer
    ref={navigatorRef => {
      NavigationService.setTopLevelNavigator(navigatorRef);
    }}
  />
);

const LocalityNavigator = () => (
  <LocalityStackContainer
    ref={navigatorRef => {
      NavigationService.setTopLevelNavigator(navigatorRef);
    }}
  />
);




// Now AppContainer is the main component for React to render

// export default AppNavigator;

/* "10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 13:00 pm","13:00 pm - 14:00 pm","14:00 pm - 15:00 pm","15:00 pm - 16:00 pm","16:00 pm - 17:00 pm","17:00 pm - 18:00 pm","18:00 pm - 19:00 pm","19:00 pm - 20:00 pm","20:00 pm - 21:00 pm","21:00 pm - 22:00 pm","22:00 pm - 23:00 pm" */
