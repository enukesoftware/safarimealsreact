import ActionTypes from '../actions/types'
import {strings} from '../../utils'

export default (state = {address:'',latitude:'',longitude:'',addressToDisplay:strings.hint_location,city:''}, action) => {
    switch (action.type) {
        case ActionTypes.CLEAR_LOCATION:
            return {address:state.address,latitude:state.latitude,longitude:state.longitude,addressToDisplay:strings.hint_location,city:state.city}
        case ActionTypes.LOCATION_SELECTED:
            return action.location
        default:
            return state
    }
}