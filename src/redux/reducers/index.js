import { combineReducers } from 'redux'
import counterReducer from './counterReducer'
import internetReducer from './internetReducer'
import locationReducer from './locationReducer'
import { dashboardTabSelectedReducer } from './tabSelectionReducer'
import { isLoginReducer, userDataReducer, deviceTokenReducer } from './userDataReducer'
import { basketReducer } from './basketReducer'
import { modifyBasketReducer } from './modifyBasketReducer'
import liveLocationReducer from './liveLocationReducer'

export default combineReducers({
    counter: counterReducer,
    internet: internetReducer,
    location: locationReducer,
    dashboardTabSelected: dashboardTabSelectedReducer,
    isLogin: isLoginReducer,
    userData: userDataReducer,
    basketData: basketReducer,
    modifyBasketData: modifyBasketReducer,
    liveLocationData: liveLocationReducer,
    deviceToken:deviceTokenReducer,
})