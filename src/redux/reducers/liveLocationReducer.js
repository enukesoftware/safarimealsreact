import ActionTypes from '../actions/types'

export default (state = null, action) => {
    switch (action.type) {
        case ActionTypes.LIVE_LOCATION:
            return action.liveLocationData
        default:
            return state
    }
}