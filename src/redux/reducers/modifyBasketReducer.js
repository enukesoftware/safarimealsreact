import ActionTypes from '../actions/types'

export const modifyBasketReducer = (state = null, action) => {
    switch (action.type) {
        case ActionTypes.ADD_ITEM_IN_MODIFY_BASKET:
            return action.item
        default:
            return state
    }
}