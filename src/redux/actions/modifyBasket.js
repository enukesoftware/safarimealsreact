import ActionTypes from './types'

export const addItemInModifyBasketAction = (item) =>{
    return{
        type:ActionTypes.ADD_ITEM_IN_MODIFY_BASKET,
        item:item
    };
}


// export const clearBasketDataAction = () =>{
//     return{
//         type:ActionTypes.BASKET_DATA,
//     };
// }