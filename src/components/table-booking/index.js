import React, { Component } from 'react'
import { TextInput, Platform, SafeAreaView, TouchableHighlight, Image, View, TouchableOpacity, Text, ActivityIndicator, Alert } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextInputLayout } from 'rn-textinputlayout';
import { ScrollView } from 'react-native-gesture-handler';
import DateTimePicker from "react-native-modal-datetime-picker";
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import moment from "moment";
import { Dialog } from 'react-native-simple-dialogs';
import RadioButton from 'react-native-radio-button'
import { bookTable, bookingHistory, cancelBooking } from '../../services/APIService'
import NavigationServices from '../../services/NavigationServices';
import { connect } from 'react-redux'


class TableBooking extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            restId: '',
            userId: '',
            name: "",
            mobile: "",
            bookingDate: "",
            bookingTime: "",
            totalGuest: "",
            isDateTimePickerVisible: false,
            isTimePickerVisible: false,
            selectedIndex: -1,
            timeOptions: [
                "10-11",
                "11-12",
                "12-13",
                "13-14",
                "14-15",
                "15-16",
                "16-17",
                "17-18",
                "18-19",
                "19-20",
                "20-21",
                "21-22",
                "22-23"
                // "10:00 - 11:00 ",
                // "11:00 - 12:00 ",
                // "12:00 - 13:00 ",
                // "13:00 - 14:00 ",
                // "14:00 - 15:00 ",
                // "15:00 - 16:00 ",
                // "16:00 - 17:00 ",
                // "17:00 - 18:00 ",
                // "18:00 - 19:00 ",
                // "19:00 - 20:00 ",
                // "20:00 - 21:00 ",
                // "21:00 - 22:00 ",
                // "22:00 - 23:00 "
            ]
            // enddate: new Date().setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000)),

        }
    }
    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }

    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.table_book} textStyle={styles.textTitle} />

                </View>
                {/* <View style={{ flexDirection: 'row',  width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginRight: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View> */}
            </View>

        )
    }

    componentDidMount() {
        const { navigation } = this.props;
        const restDetail = navigation.getParam('restDetail', '');
        if (this.props.userData) {
            const userData = this.props.userData
            userId = (userData.id) ? userData.id : ""
            this.setState({
                restId: restDetail.id,
                userId:userId
            })
        }

    }

    _selectSelected = (index) => {
        if (index == this.state.selectedIndex)
            return true
        else
            return false
    }

    _dialogTime = () => {
        return this.state.timeOptions.map((item, index) => (
            <TouchableOpacity
                onPress={() => this.setState({
                    selectedIndex: index,
                    bookingTime: item,
                    isTimePickerVisible: false
                })}
                key={index} style={styles.touchItemDialoge}>

                {/* View For RadioButton */}
                <View style={styles.viewRadio}>
                    <RadioButton
                        outerColor={Constants.color.primary}
                        innerColor={Constants.color.primary}
                        onPress={() => this.setState({
                            selectedIndex: index,
                            bookingTime: item,
                            isTimePickerVisible: false
                        })}
                        isSelected={this._selectSelected(index)}
                    />
                </View>

                {/* TIME TEXT */}
                <TextRegular title={item.trim()} textStyle={{ width: '80%' }} />
            </TouchableOpacity>
        ))
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
        this.setState({  //bookingDate:date.getFullYear().toString()+"/"+(date.getMonth() + 1).toString()+"/"+date.getDate().toString(),
            bookingDate: moment(date).format('YYYY-MM-DD')
        })

        this.hideDateTimePicker();
    };
    //function for verify and Register
    _verifyAndRegister = () => {
  
        if (this.state.name == "") {
            alert(strings.full_name_error)
            return
        }
        if (this.state.name.length < 4) {
            alert(strings.correct_name_error)
            return
        }
        if (!isNaN(this.state.name)) {
            alert(strings.correct_name_error)
            return
        }
        if (this.state.mobile == "") {
            alert(strings.phone)
            return
        }
        if (this.state.mobile.length < 10 || isNaN(this.state.mobile)) {
            alert(strings.phone_no_error)
            return
        }
        if (this.state.totalGuest == "") {
            alert(strings.guest_error)
            return
        }
        if (this.state.bookingDate == "") {
            alert(strings.date_picker_error)
            return
        }
        if (this.state.bookingTime == "") {
            alert(strings.time_picker_error)
            return
        }
        //here u do Book Table
        this.callBookTableApi()
    }

    onAlertOkClick = (success) => {
        if (success) {
            NavigationServices.goBack()
        }
    }

    showBookTableResponseAlert = (success) => {
        Alert.alert(
            strings.book_table,
            success ? strings.table_booking_request_generated : strings.table_booking_request_not_generated,
            [
                //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                //   {
                //     text: strings.cancel,
                //     onPress: () => console.log('Cancel Pressed'),
                //     style: 'cancel',
                //   },
                {
                    text: strings.ok,
                    onPress: () => this.onAlertOkClick(success)
                },
            ],
            { cancelable: false },
        );
    }

    callBookTableApi = () => {
        if(!this.props.internet){
                    alert(strings.message_lno_internet)
                    return
                }
        this.setState({
            isLoading: true,
        })

        let param = {
            customer_id: this.state.userId,
            customer_name: this.state.name,
            customer_contact: this.state.mobile,
            total_person: this.state.totalGuest,
            book_date: this.state.bookingDate,
            book_time: this.state.bookingTime,
            restaurant_id: this.state.restId,

        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.bookTable


        console.log("URL:" + url + " PARAM:" + JSON.stringify(param))
        bookTable(url, param).then(res => {

            console.log("BOOK_TABLE_API_RES:" + JSON.stringify(res))


            if (res && res.success) {
                this.setState({
                    isLoading: false,
                }, () => {
                    this.showBookTableResponseAlert(true)
                })
            } else {
                this.setState({
                    // noDataFound: true,
                    isLoading: false,
                }, () => {
                    this.showBookTableResponseAlert(false)
                })
            }



            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                // noDataFound: true,
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    // err = JSON.parse(err)
                    // alert(err);
                    this.showBookTableResponseAlert(false)
                }
            }, 100);
        });
    }


    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={styles.activityIndicatorView}>
                    <View style={styles.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={this.state.loading} />
                    </View>
                </View>

            )
        } else {
            return (
                <View />
            )
        }

    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: Constants.color.white }} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                {this.renderProgressBar()}

                {this.renderToolbar()}

                <View style={styles.loginForm}>
                    <ScrollView contentInsetAdjustmentBehavior="automatic"
                        keyboardShouldPersistTaps='always'
                        style={{}}>

                        {/*Name*/}
                        <View style={styles.viewInput}>
                            <Image source={Images.ic_person} style={styles.iconLeft}></Image>
                            <TextInputLayout
                                style={styles.inputLayout}
                                focusColor={Constants.color.black}>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={(text) => {
                                        this.setState({
                                            name: text.trim()
                                        });
                                    }}
                                    placeholder={strings._hint_first_name} />

                            </TextInputLayout>
                        </View>

                        {/* Contact */}
                        <View style={styles.viewInput}>
                            <Image source={Images.phone} style={styles.iconLeft}></Image>
                            <TextInputLayout
                                style={styles.inputLayout}
                                focusColor={Constants.color.black}>
                                <TextInput
                                    maxLength={10}
                                    ref='mobileNo'
                                    keyboardType="numeric"
                                    onChangeText={(text) => {
                                        this.setState({
                                            mobile: text.trim()
                                        });
                                    }}
                                    style={styles.textInput}
                                    placeholder={strings.hint_contact_number} />
                            </TextInputLayout>
                        </View>

                        {/* Total Guest*/}
                        <View style={styles.viewInput}>
                            <Image source={Images.ic_guests} style={styles.iconLeft}></Image>
                            <TextInputLayout
                                style={styles.inputLayout}
                                focusColor={Constants.color.black}>
                                <TextInput
                                    maxLength={2}
                                    ref='mobileNo'
                                    keyboardType="numeric"
                                    onChangeText={(text) => {
                                        this.setState({
                                            totalGuest: text
                                        });
                                    }}
                                    style={styles.textInput}
                                    placeholder={strings.hint_total_guest} />
                            </TextInputLayout>
                        </View>

                        {/*Booking date*/}
                        <View
                            style={styles.viewInput}>
                            <Image source={Images.calender} style={styles.iconLeft}></Image>
                            <TouchableOpacity style={styles.inputLayout}
                                onPress={() => { this.showDateTimePicker() }}>
                                <TextInputLayout
                                    focusColor={Constants.color.black}>
                                    <TextInput
                                        pointerEvents="none"
                                        editable={false}
                                        value={this.state.bookingDate}
                                        style={styles.textInput}
                                        placeholder={strings.hint_booking_date} />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                mode="date"
                                minimumDate={new Date()}
                                maximumDate={new Date(moment(new Date()).add(7, 'day').format('YYYY-MM-DD'))}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.hideDateTimePicker}
                            />

                        </View>

                        {/*Booking time*/}
                        <View style={styles.viewInput}>
                            <Image source={Images.clock} style={styles.iconLeft}></Image>
                            <TouchableOpacity style={styles.inputLayout} onPress={() => {
                                this.setState({
                                    isTimePickerVisible: true
                                })
                            }}>
                                <TextInputLayout
                                    focusColor={Constants.color.black}>
                                    <TextInput
                                        pointerEvents="none"
                                        editable={false}
                                        value={this.state.bookingTime}
                                        style={styles.textInput}
                                        placeholder={strings.hint_booking_time} />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <Dialog
                                visible={this.state.isTimePickerVisible}
                                title={strings.select_booking_time}
                                titleStyle={styles.titleStyle}
                                onTouchOutside={() => this.setState({ isTimePickerVisible: false })} >
                                {this._dialogTime()}
                            </Dialog>
                        </View>

                        {/*Button Table Book */}
                        <View style={styles.viewButton}>
                            <TouchableOpacity
                                onPress={() => {
                                    this._verifyAndRegister();
                                }}
                                style={styles.touchOpacity}>
                                <TextBold title={strings.table_book} textStyle={styles.textButtonAdd} />
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </View>

        );
    }
}

function mapStateToProps(state) {
    console.log("userData:" + JSON.stringify(state.userData))
    return {
        userData: state.userData,
        internet:state.internet,
        isLogin: state.isLogin,
    }
}

export default connect(mapStateToProps, {})(TableBooking)








