import { Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        alignItems: 'center', justifyContent: 'center'
    },
    loginForm: {
        padding: 35,
        width: '100%'
    },
    weekRow: {
        flexDirection: "row",
        marginStart: 25,
        marginEnd: 25,
        alignItems: 'center',
        marginVertical: 10,
        width: Constants.Screen.width - 50,
    },
    textOrder: {
        color: 'white',
        fontSize: 15,
        marginHorizontal: 10,
    },

    viewHead: {
        backgroundColor: Constants.color.white,
        width: '100%',
        height: 2,
    },
    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 15,
    },

    imageHash: {
        width: 30,
        height: 30
    },
    viewImageBackground: {
        flex: 1,
        justifyContent: "flex-end",
        margin: 15
    },
    tstatus: {
        flex: 1,
        fontSize: 12,
        textAlign: "center"
    },
    venable: {
        backgroundColor: Constants.color.green_background,
        height: 4,
        width: '16%'
    },
    vdisable: {
        backgroundColor: Constants.color.line_color,
        height: 4,
        width: '16%'
    },
    viewImagesDisabled: {
        width: '13%',
        height: (Constants.Screen.width - 50) * (13 / 100),
        borderRadius: (Constants.Screen.width - 50) * (13 / 200),
        backgroundColor: 'white',
        borderColor: Constants.color.line_color,
        borderWidth: 2,
        alignItems: "center",
        justifyContent: "center"
    },
    viewImagesEnabled: {
        width: '13%',
        height: (Constants.Screen.width - 50) * (13 / 100),
        borderRadius: (Constants.Screen.width - 50) * (13 / 200),
        backgroundColor: Constants.color.primary,
        alignItems: "center",
        justifyContent: "center"
    },
    driverContactCard: {
        borderColor: Constants.color.white,
        borderBottomColor: Constants.color.gray,
        borderWidth: 1.5,
        padding: 2,
        margin: 10,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5
    },
    restImage: {
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        resizeMode: 'contain',
    },
    callImageView: {
        height: 40,
        width: 40,
        borderRadius: 40 / 2,
        alignItems: "center",
        justifyContent: 'center',
        backgroundColor: Constants.color.primary,
    },
    itemOrderText: {
        fontSize: 20,
        marginHorizontal: 25,
        color: Constants.color.black
    },
    itemOrderView: {
        flexDirection: "row",
        marginEnd: 25,
        paddingVertical: 5
    },
    processView: {
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 5,
        padding: 10,
        paddingRight: 20,
        backgroundColor: Constants.color.primary,
        flexDirection: "row",
    },
    img: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
    menuInfoImage: {
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 0,
        borderColor: Constants.color.primary,
    },

})