import React, { Component } from 'react'
import { ActivityIndicator, Platform, TextInput, SafeAreaView, TouchableHighlight, Image, View, TouchableOpacity, Text, WebView } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { TextInputLayout } from 'rn-textinputlayout';
import { ScrollView } from 'react-native-gesture-handler';


class ContactUs extends Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: true
    }
  }

  onBackClick = () => {
    const { navigation } = this.props;
    navigation.goBack();
  }


  renderBackButton() {
    if (Platform.OS === Constants.PLATFORM.ios) {
      return (
        <TouchableOpacity activeOpacity={0.8}
          onPress={() => this.onBackClick()}>
          <Image source={Images.ic_back_ios}
            style={{ marginLeft: 5, height: 22, width: 22, }}
          ></Image>
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity activeOpacity={0.8}
          onPress={() => this.onBackClick()}>
          <Image source={Images.ic_back_android}
            style={{ marginLeft: 15, height: 35, width: 25, }}
          ></Image>
        </TouchableOpacity>
      )
    }
  }


  renderToolbar() {
    return (
      <View style={styles.toolbar}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: '90%'
        }
        }>
          {this.renderBackButton()}
          <TextBold title={strings.side_menu_contact_us} textStyle={styles.textTitle} />

        </View>

      </View>

    )
  }
  hideSpinner() {
    this.setState({ visible: false });
  }
  render() {

    return (
      <View style={{ flex: 1 }}>
        <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
        <View style={{ flex: 1, backgroundColor: Constants.color.white }} >
          {this.renderToolbar()}
          <WebView
            onLoad={() => this.hideSpinner()}
            source={{ uri: Constants.URL.CONTACT_US_URL }}
          />
          {this.state.visible && (
            <View style={styles.activityIndicatorView}>
              <View style={styles.activityIndicatorWrapper}>
                <ActivityIndicator
                  size={"large"}
                  color={Constants.color.primary}
                  animating={this.state.loading} />
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default ContactUs







