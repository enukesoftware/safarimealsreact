import { Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({

  toolbar: {
    height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Constants.color.primary,
    shadowOffset: { width: 10, height: 2 },
    shadowOpacity: 0.2,
    elevation: 15
  },
  textTitle: {
    fontSize: 20,
    color: Constants.color.white,
    marginLeft: 15,
  },
  activityIndicatorView: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: '15%'
  },
  activityIndicatorWrapper: {
    // position:'absolute',
    // top:'35%',
    // left:'38%',
    backgroundColor: Constants.color.gray,
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.8,
  },


})
