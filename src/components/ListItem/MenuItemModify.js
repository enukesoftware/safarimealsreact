import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, StyleSheet, View, Alert,AsyncStorage,Image } from 'react-native';
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import CustomAddItem from '../custom/custom-add-item'
import NavigationService from '../../services/NavigationServices'
import { addItemInModifyBasketAction,  } from '../../redux/actions'


class MenuItemModify extends Component {

    constructor() {
        super()
    }

    componentWillMount() {
        this.state = {
            quantity: this.props.value.quantity
        }
    }

    // componentDidUpdate(){
    //     console.log("MENU_ITEM_DID_MOUNT" + this.props.value.quantity)
    //     this.setState ({
    //         quantity: this.props.value.quantity
    //     })
    // }

    componentWillReceiveProps() {
        // console.log("MENU_ITEM_DID_MOUNT" + this.props.value.quantity)
        // this.setState ({
        //     quantity: this.props.value.quantity
        // })
    }

    showBasketAlert = () => {
        Alert.alert(
            strings.title_basket,
            strings.cart_dialog,
            [
                //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {
                    text: strings.cancel,
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: strings.ok,
                    onPress: () => this.clearBasket()
                },
            ],
            { cancelable: false },
        );
    }

    clearBasket = () => {
        this.props.addItemInModifyBasketAction(null)
        this.props.value.quantity = this.props.value.quantity + 1
        this.setState({
            quantity: this.props.value.quantity
        }, () => {
            this.initializeBasket()
        })
    }

    initializeBasket = () => {
        const restDetail = this.props.restDetail
        const restId = this.props.restDetail.id
        const totalQuantity = this.state.quantity
        const totalPrice = this.state.quantity * parseInt(this.props.value.cost)
        const totalCgst = totalQuantity * parseInt(restDetail.cgst)
        const totalSgst = totalQuantity * parseInt(restDetail.sgst)
        let product = this.props.value

        product = {
            ...product,
            quantity: this.state.quantity,
            productPrice: this.state.quantity * parseInt(this.props.value.cost),
            unitPrice: parseInt(this.props.value.cost)

        }

        const basket = {
            restDetail: restDetail,
            restId: restId,
            totalQuantity: totalQuantity,
            totalPrice: totalPrice,
            totalCgst: totalCgst,
            totalSgst: totalSgst,
            products: [product]
        }

        this.props.addItemInModifyBasketAction(basket)
        //AsyncStorage.setItem(Constants.STORAGE_KEY.basketData, JSON.stringify(basket));
    }

    addItemInAlreadyFilledBasket = () => {
        const restDetail = this.props.restDetail
        const restId = this.props.restDetail.id
        const totalQuantity = this.props.basketData.totalQuantity + 1
        const totalPrice = this.props.basketData.totalPrice + parseInt(this.props.value.cost)
        const totalCgst = totalQuantity * parseInt(this.props.restDetail.cgst)
        const totalSgst = totalQuantity * parseInt(this.props.restDetail.sgst)
        let product = this.props.value

        product = {
            ...product,
            quantity: this.state.quantity,
            productPrice: this.state.quantity * parseInt(this.props.value.cost),
            unitPrice: parseInt(this.props.value.cost),
        }



        var alreadyAddedProductsInBasket = [...this.props.basketData.products]; // make a separate copy of the array
        let productIndex = -1
        alreadyAddedProductsInBasket.forEach((element, index) => {
            if (element.id === product.id) {
                productIndex = index
            }
        });

        let products = []
        if (productIndex != -1) {
            // alreadyAddedProductsInBasket.splice(productIndex, 1);
            alreadyAddedProductsInBasket[productIndex] = product
            products = alreadyAddedProductsInBasket
        } else {
            if (alreadyAddedProductsInBasket && alreadyAddedProductsInBasket.length > 0) {
                products = [...alreadyAddedProductsInBasket, ...[product]]
            } else {
                products = [product]
            }
        }

        const basket = {
            restDetail: restDetail,
            restId: restId,
            totalQuantity: totalQuantity,
            totalPrice: totalPrice,
            totalCgst: totalCgst,
            totalSgst: totalSgst,
            products: products
        }

        this.props.addItemInModifyBasketAction(basket)
        //AsyncStorage.setItem(Constants.STORAGE_KEY.basketData, JSON.stringify(basket));
    }

    getTotalPrice = (isQuntitiyIncluded, product) => {
        let productPrice = parseInt(product.cost)
        let addOnPrice = 0

        if (product.selectedAddOnsGroups) {
            product.selectedAddOnsGroups.forEach(element => {
                let addOns = product.selectedAddOns[element]
                addOns.forEach(item => {
                    addOnPrice += parseInt(item.price)
                });
            });
        }


        if (isQuntitiyIncluded) {
            return (this.state.quantity * (productPrice + addOnPrice))

        } else {
            return (productPrice + addOnPrice)
        }


    }

    removeItemFromBasket = () => {
        const restDetail = this.props.restDetail
        const restId = this.props.restDetail.id
        const totalQuantity = this.props.basketData.totalQuantity - 1
        let totalPrice = 0//this.props.basketData.totalPrice - this.getTotalPrice(false)

        // parseInt(this.props.value.cost)
        const totalCgst = totalQuantity * parseInt(this.props.restDetail.cgst)
        const totalSgst = totalQuantity * parseInt(this.props.restDetail.sgst)
        let product = this.props.value


        product = {
            ...product,
            quantity: this.state.quantity,
            productPrice: this.getTotalPrice(true, product),
            unitPrice: this.getTotalPrice(false, product),
        }



        var alreadyAddedProductsInBasket = [...this.props.basketData.products]; // make a separate copy of the array
        let productIndex = -1
        alreadyAddedProductsInBasket.forEach((element, index) => {
            if (element.id === product.id) {
                productIndex = index
                totalPrice = this.props.basketData.totalPrice - element.unitPrice
                if (element.selectedAddOnsGroups && element.selectedAddOnsGroups.length > 0) {
                    product = {
                        ...product,
                        productPrice: this.getTotalPrice(true, element),
                        unitPrice: this.getTotalPrice(false, element),
                        selectedAddOns: element.selectedAddOns,
                        selectedAddOnsGroups: element.selectedAddOnsGroups
                    }
                }
            }
        });

        let products = []
        if (productIndex != -1) {
            // alreadyAddedProductsInBasket.splice(productIndex, 1);
            if (this.state.quantity > 0) {
                alreadyAddedProductsInBasket[productIndex] = product
            }else{
                alreadyAddedProductsInBasket.splice(productIndex, 1);
            }
            products = alreadyAddedProductsInBasket
        } else {
            if (this.state.quantity > 0) {
                products = [...alreadyAddedProductsInBasket, ...[product]]
            } else {
                products = alreadyAddedProductsInBasket
            }
        }

        const basket = {
            restDetail: restDetail,
            restId: restId,
            totalQuantity: totalQuantity,
            totalPrice: totalPrice,
            totalCgst: totalCgst,
            totalSgst: totalSgst,
            products: products
        }

        if (totalQuantity == 0) {
            this.props.addItemInModifyBasketAction(null)
            //AsyncStorage.removeItem(Constants.STORAGE_KEY.basketData)
        } else {
            this.props.addItemInModifyBasketAction(basket)
            //AsyncStorage.setItem(Constants.STORAGE_KEY.basketData, JSON.stringify(basket));
        }
    }

    addItem = () => {
        // this.props.value.quantity = this.props.value.quantity + 1
        // this.setState({
        //     quantity: this.props.value.quantity
        // })

        if (this.props.value.addons && this.props.value.addons.length > 0) {
            NavigationService.navigate('AddOnsModify', {
                menuName: this.props.menuName,
                product: this.props.value,
                restDetail: this.props.restDetail
            })
        } else {

            if (!this.props.basketData) {
                this.props.value.quantity = this.props.value.quantity + 1
                this.setState({
                    quantity: this.props.value.quantity
                }, () => {
                    this.initializeBasket()
                })

            } else if (this.props.basketData.restId != this.props.restDetail.id) {
                this.showBasketAlert()
            } else {
                this.props.value.quantity = this.props.value.quantity + 1
                this.setState({
                    quantity: this.props.value.quantity
                }, () => {
                    this.addItemInAlreadyFilledBasket()
                })

            }


        }


        // console.log("ADD:" + this.props.value.quantity)
    }

    removeItem = () => {
        if (this.props.value.quantity != 0) {
            this.props.value.quantity = this.props.value.quantity - 1
            console.log("QUANTITY:" + this.props.value.quantity)
            this.setState({
                quantity: this.props.value.quantity
            }, () => {
                this.removeItemFromBasket()
            })
        }
    }

    changeQuantity = key => value => {
        console.log("KEY:" + key + " VALUE:" + value)
        switch (key) {
            case 'add':
                this.addItem()
                break
            case 'remove':
                this.removeItem()
                break

        }
    }

    renderQuantity = () => {
        return (
            <CustomAddItem
                quantity={(this.state.quantity) ? this.state.quantity : 0}
                addItem={this.changeQuantity('add')}
                removeItem={this.changeQuantity('remove')} />
        )
    }

    render() {
        return (
            <View style={styles.container}>

                {/* ---------- Item Data ---------- */}
                <View style={styles.itemView}>

                    {/* ---------- Left View ---------- */}
                    <View style={styles.leftView}>
                        <View>
                            <Image source={(this.props.value && this.props.value.image && this.props.value.image.location && this.props.value.image.location != "") ? { uri: this.props.value.image.location } : Images.no_product} style={styles.menuInfoImage}></Image>
                        </View>
                        <View style={{paddingLeft:10,paddingRight:30}}>
                            <TextBold title={this.props.value.name} textStyle={styles.itemText} />
                            <TextRegular title={Constants.currency.dollar + ' ' + this.props.value.cost} textStyle={styles.priceText} />
                        </View>

                    </View>
                    {/* ---------- Right View ---------- */}
                    <View style={styles.rightView}>
                        {this.renderQuantity()}
                    </View>
                </View>


                {/* ---------- Divider ---------- */}
                <View style={styles.dividerView} />
            </View>


        );
    };
}

function mapStateToProps(state) {
    // console.log("MENU_ITEM_STATE:", JSON.stringify(state))
    return {
        basketData: state.modifyBasketData,

    }
}

export default connect(mapStateToProps, { addItemInModifyBasketAction })(MenuItemModify)

const styles = StyleSheet.create({
    container: {
        width: '100%',
        paddingTop: 10,
        marginBottom: -1,
    },
    itemView: {
        flexDirection: 'row',
    },
    leftView: {
        width: '68%',
        flexDirection: 'row',
    },
    itemText: {
        opacity: 0.8,
    },
    priceText: {
        opacity: 0.7,
    },
    rightView: {
        width: '32%',
        alignItems: 'flex-end',
    },
    dividerView: {
        height: 1,
        width: '100%',
        backgroundColor: Constants.color.gray,
        marginTop: 10,
    },
    menuInfoImage: {
        width: 30,
        height: 30,
        borderRadius: 15,
        borderWidth: 0,
        borderColor: Constants.color.primary,
    },
})