import { Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({

    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 15,
    },
    viewSep: {
        backgroundColor: Constants.color.black,
        height: 0.4,
        marginTop: 10

    },
    viewInput: {
        paddingTop: 5,
        flexDirection: "row",
        alignItems: 'flex-end',
    },

    TouchClose: {
        height: 35,
        width: 35,
        alignItems: "center",
        justifyContent: "center"
    },
    TextFilterNavigation: {
        fontSize: 20,
        color: 'white',
        fontWeight: '500'
    },
    mainForm: {
        width: '100%',
        flex: 1
    },
    viewCenter: {
        alignItems: "center",
        justifyContent: "center",
    },
    borderView: {
        alignItems: "center",
        justifyContent: "center",
        height: 100,
        width: 100,
        borderColor: Constants.color.placeholder_grey,
        borderRadius: 50,
        overflow: 'hidden',
        borderWidth: 2
    },
    profileImage: {
        width: 100,
        height: 100,
        borderRadius:50,
    },
    textHeading: {
        color: Constants.color.primary,
        fontSize: 16,
    },
    textData: {
        flex: 1,
        padding: 5,
        color: Constants.color.placeholder_grey,
        fontSize: 14
    },
    textInput: {
        fontSize: 18,
        height: 40,
        fontFamily: Fonts.Regular,
        width: '100%',
        paddingLeft:8
    },
    inputLayout: {
        width: '80%',
        marginHorizontal: 10
    },
    iconLeft: {
        height: 30,
        width: 30,
        margin: 5,
        
    },
    viewSingleData: {
        flex: 1,
        paddingTop: 10,
        flexDirection: "row",
        alignItems: 'center',
    },
    imgBack: {
        width: '100%',
        height: '100%'
    },
    imgEdit: {
        width: '50%',
        height: '50%'
    },
    touchOpacity: {
        backgroundColor: Constants.color.primary,
        justifyContent: "center",
        alignItems: "center",
        width: '100%',
        padding: 15,
        borderRadius: 5
    },
    textButtonAdd: {
        color: Constants.color.white,
        fontSize: 16,
    },
    viewButton: {
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: 30,
    },
    countryCodeInput:{
        fontSize: 18,
        fontFamily: Fonts.Regular,
        width: '100%',
        paddingLeft:8,
    }


})
