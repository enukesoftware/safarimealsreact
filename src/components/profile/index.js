import React, { Component } from 'react'
import { Platform, TextInput, SafeAreaView, ActivityIndicator, Image, View, TouchableOpacity, AsyncStorage, } from 'react-native';
import styles from './styles'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextInputLayout } from 'rn-textinputlayout';
import { ScrollView } from 'react-native-gesture-handler'
import ImagePicker from 'react-native-image-picker'
import { connect } from 'react-redux'
import { profileUpdateApi } from '../../services/APIService'
import NavigationService from '../../services/NavigationServices';
import { userDataAction } from '../../redux/actions'
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class Profile extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            editable: false,
            id: '',
            email: '',
            newEmail: '',
            mobile: '',
            firstName: '',
            lastName: '',
            newmobile: '',
            newFirstName: '',
            newLastName: '',
            profileImage: Images.ic_avatar,
            newprofileImage: Images.ic_avatar,
            isLoading: false,
            countrycode: Constants.DEFAULT_COUNTRY_CODE,
            newCountryCode: Constants.DEFAULT_COUNTRY_CODE,
            isImageSelected: false
        }
    }

    componentDidMount() {
        if (this.props.userData) {
            const userData = this.props.userData
            console.log("USER_DATA_PROFILE:" + JSON.stringify(this.props.userData))
            userImage = (userData.profile_image && userData.profile_image != "") ? { uri: userData.profile_image } : Images.ic_avatar

            if (userData.first_name) {
                firstName = userData.first_name
            }
            if (userData.last_name) {
                lastName = userData.last_name
            }

            userEmail = (userData.email) ? userData.email : ""
            userMobile = (userData.contact_number) ? userData.contact_number : ""
            userId = (userData.id) ? userData.id : ""
            countrycode = (userData.countrycode) ? userData.countrycode : Constants.DEFAULT_COUNTRY_CODE
            this.setState({
                email: userEmail,
                newEmail: userEmail,
                firstName: firstName,
                lastName: lastName,
                newFirstName: firstName,
                newLastName: lastName,
                mobile: userMobile,
                id: userId,
                newmobile: userMobile,
                newprofileImage: userImage,
                profileImage: userImage,
                countrycode: countrycode,
                newCountryCode: countrycode,
            })

        }

    }


    onBackClick = () => {
        const { navigation } = this.props;
        if (this.state.editable) {
            this.setState({
                editable: !this.state.editable,
                newprofileImage: this.state.profileImage,
                newmobile: this.state.mobile,
                newFirstName: this.state.firstName,
                newLastName: this.state.lastName,
                newCountryCode: this.state.countrycode,
                isImageSelected: false,
                newEmail: this.state.email,
            })
        } else
            navigation.goBack();
    }

    onCountrySelect = data => {
        this.setState({
            newCountryCode: data.selectedCountry.countryCode,
        })
    };


    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }

    showImagePicker = () => {
        // return
        // this.setState({ loading: true });
        const options = {
            rotation: 360,
            allowsEditing: true,
            noData: true,
            mediaType: "photo",
            maxWidth: 300,
            maxHeight: 300,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, response => {
            console.log("Response = ", response);
            // this.setState({ loading: false });
            if (response.didCancel) {
                //   console.log(JSON.stringify(source));
                console.warn("User cancelled image picker");
            } else if (response.error) {
                //  console.log(JSON.stringify(source));
                console.warn("ImagePicker Error: ", response.error);
            } else if (response.customButton) {
                //  console.log('User tapped custom button: ', response.customButton);
            } else {

                const source = { uri: response.uri };

                let fileName = "profile_picture.jpg"
                // if (response && response.fileName) {
                //   let extension = response.fileName.substr(response.fileName.lastIndexOf('.'));
                //   if (extension && extension != "") {
                //     fileName = this.state.userData.id + "_" + response.fileName
                //   }

                // }

                if (response && response.uri) {
                    let extension = response.uri.substr(response.uri.lastIndexOf('.'));
                    if (extension && extension != "") {
                        // fileName = this.state.userData.id + "_" + response.fileName
                        fileName = 'profile_picture_' + this.state.id + extension
                    }

                }

                console.log("RES:" + JSON.stringify(response))
                console.log("FILE:" + JSON.stringify(fileName))


                const myImg = {
                    uri: response.uri,
                    type: "image/jpeg",
                    name: fileName
                };

                this.setState({
                    newprofileImage: myImg,
                    isImageSelected: true,
                });
            }
        });
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}

                    <TextBold title={
                        this.state.editable ?
                            strings.edit_profile : strings.profile_text} textStyle={styles.textTitle} />

                </View>
                {<View style={{ flexDirection: 'row', width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                        onPress={() => {
                            this.setState({
                                editable: !this.state.editable,
                                newmobile: this.state.mobile,
                                newprofileImage: this.state.profileImage,
                                newFirstName: this.state.firstName,
                                newLastName: this.state.lastName,
                                newCountryCode: this.state.countrycode,
                                isImageSelected: false,
                                newEmail: this.state.email,
                            })
                        }}
                        style={{ width: '100%', justifyContent: "center", alignItems: "center" }}>
                        <Image resizeMode={'contain'}
                            source={
                                this.state.editable ?
                                    Images.ic_close_white : Images.ic_edit_white}
                            style={{ marginRight: 15, height: this.state.editable ? 25 : 17, width: this.state.editable ? 25 : 17 }}
                        ></Image>
                    </TouchableOpacity>
                </View>}
            </View>

        )
    }

    _addressBookClicked() {
        const { navigation } = this.props;
        navigation.navigate('SelectDeliveryAddress');
    }
    _verifyAndUpdate = () => {
        let emailcontext = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (!this.state.newFirstName || this.state.newFirstName == "") {
            alert(strings.firstName_error)
            return
        }

        if (!this.state.newLastName || this.state.newLastName == "") {
            alert(strings.lastNamed_error)
            return
        }

        if (this.state.newEmail == "") {
            alert(strings.email_error)
            return
        }

        if (!emailcontext.test(this.state.newEmail)) {
            alert(strings.email_id_error)
            return
        }

        if (!this.state.newCountryCode || this.state.newCountryCode == "") {
            alert(strings.countrycode)
            return
        }

        if (this.state.newmobile == "") {
            alert(strings.phone)
            return
        }
        if (this.state.newmobile.length < 10 || isNaN(this.state.newmobile)) {
            alert(strings.correct_mobile_number_error)
            return
        }

        this.callUpdateProfile()

    }

    // _imagePicker = () => {
    //     ImagePicker.openPicker({
    //         height: 300,
    //         width: 300,
    //         cropping: true
    //     }).then(image => {
    //         const p = { uri: image.path }
    //         //  console.warn(p)
    //         this.setState({
    //             newprofileImage: p,
    //         })
    //     });
    // }


    callUpdateProfile() {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }
        // const param = {
        //     id: this.state.id,
        //     contact_number: this.state.newmobile,
        //     first_name: this.state.newFirstName,
        //     last_name: this.state.newLastName,
        //     email: this.state.email,
        //     countrycode: this.state.newCountryCode,
        // }

        let param = new FormData();

        if (this.state.isImageSelected) {
            param.append("image", this.state.newprofileImage);
        }
        param.append("id", this.state.id);
        param.append("first_name", this.state.newFirstName);
        param.append("last_name", this.state.newLastName);
        param.append("email", this.state.newEmail);
        param.append("countrycode", this.state.newCountryCode);
        param.append("contact_number", this.state.newmobile);

        this.setState({
            isLoading: true,
        })
        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.profileUpdate

        profileUpdateApi(url, param).then(res => {
            // this.setState({
            //     isLoading: false,
            // })
            // console.log("UPDATE_RES1:" + JSON.stringify(res))
            if (res && res.success) {
                let profileImage = Images.ic_avatar
                if (res.data && res.data.profile_image) {
                    profileImage = { uri: res.data.profile_image }
                }
                try {
                    this.setState({
                        mobile: this.state.newmobile,
                        isLoading: false,
                        editable: false,
                        firstName: this.state.newFirstName,
                        lastName: this.state.newLastName,
                        countrycode: this.state.newCountryCode,
                        profileImage: profileImage,
                        email: this.state.newEmail,
                    }, () => {
                        if (this.state.isImageSelected && res.data && res.data.profile_image) {
                            this.props.userDataAction({
                                ...this.props.userData,
                                contact_number: this.state.newmobile,
                                first_name: this.state.newFirstName,
                                last_name: this.state.newLastName,
                                countrycode: this.state.newCountryCode,
                                profile_image: res.data.profile_image,
                                email: this.state.newEmail,
                            })
                            this.props.userData['contact_number'] = this.state.newmobile,
                                this.props.userData['first_name'] = this.state.newFirstName,
                                this.props.userData['last_name'] = this.state.newLastName,
                                this.props.userData['countrycode'] = this.state.newCountryCode,
                                this.props.userData['profile_image'] = res.data.profile_image,
                                this.props.userData['email'] = this.state.newEmail
                        } else {
                            this.props.userDataAction({
                                ...this.props.userData,
                                contact_number: this.state.newmobile,
                                first_name: this.state.newFirstName,
                                last_name: this.state.newLastName,
                                countrycode: this.state.newCountryCode,
                                email: this.state.newEmail,
                            })
                            this.props.userData['contact_number'] = this.state.newmobile,
                                this.props.userData['first_name'] = this.state.newFirstName,
                                this.props.userData['last_name'] = this.state.newLastName,
                                this.props.userData['countrycode'] = this.state.newCountryCode,
                                this.props.userData['email'] = this.state.newEmail
                        }

                        console.log("USER_DATA_NEW:" + JSON.stringify(this.props.userData))
                        AsyncStorage.setItem(Constants.STORAGE_KEY.userData, JSON.stringify(this.props.userData));

                    })
                } catch (e) {
                    alert(e)
                }

                /*   this.setState({
                      mobile:this.props.userData.contact_number
                  }) */
                alert(res.message);




            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    }else if(res && res.message){
                        alert(res.message)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });
    }

    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>

            )
        } else {
            return
        }

    }
    render() {



        return (
            <View style={{ flex: 1 }}  >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}>
                </SafeAreaView>
                <View style={{ flex: 1, backgroundColor: Constants.color.white }}  >
                    {this.renderToolbar()}
                    <View style={styles.mainForm} >
                        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ width: '100%', flex: 1 }} >
                            {/* Profile Image*/}
                            <View style={{ padding: 20, flex: 1, marginBottom: 100 }}>
                                {
                                    this.state.editable ?
                                        <View style={styles.viewCenter} >
                                            <TouchableOpacity style={styles.borderView} onPress={() => { this.showImagePicker() }}>
                                                <Image
                                                    source={(this.state.newprofileImage && this.state.newprofileImage.uri) ? { uri: this.state.newprofileImage.uri } : this.state.profileImage}
                                                    style={styles.profileImage} resizeMode="cover"></Image>
                                            </TouchableOpacity>
                                        </View>
                                        :
                                        <View style={styles.viewCenter} >
                                            <View style={styles.borderView}>
                                                <Image source={this.state.profileImage} style={styles.profileImage} resizeMode="cover"></Image>
                                            </View>
                                        </View>
                                }

                                {/* Profile Name*/}
                                {/* <View style={styles.viewCenter}>
                                <TextBold title={this.state.first_name + " " + this.state.last_name} textStyle={{ padding: 15 }} />
                            </View> */}
                                {/*Seperater Line*/}
                                <View style={styles.viewSep}></View>
                                {/* First Name*/}
                                {this.state.editable ?
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.ic_person} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.first_name_text} textStyle={styles.textHeading} />
                                        <TextInput
                                            ref='firstName'
                                            keyboardType="default"
                                            onChangeText={(text) => {
                                                this.setState({
                                                    newFirstName: text.trim()
                                                });
                                            }}
                                            style={styles.textInput}
                                            value={this.state.newFirstName}
                                            placeholder={strings._hint_first_name} />
                                    </View>
                                    :
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.ic_person} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.first_name_text} textStyle={styles.textHeading} />

                                        <TextRegular title={this.state.firstName} textStyle={styles.textData} />
                                    </View>

                                }

                                {/*Seperater Line*/}
                                <View style={styles.viewSep}></View>
                                {/* Last Name*/}
                                {this.state.editable ?
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.ic_person} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.last_name_text} textStyle={styles.textHeading} />
                                        <TextInput
                                            ref='lastName'
                                            keyboardType="default"
                                            onChangeText={(text) => {
                                                this.setState({
                                                    newLastName: text.trim()
                                                });
                                            }}
                                            style={styles.textInput}
                                            value={this.state.newLastName}
                                            placeholder={strings.hint_last_name} />
                                    </View>
                                    :
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.ic_person} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.last_name_text} textStyle={styles.textHeading} />

                                        <TextRegular title={this.state.lastName} textStyle={styles.textData} />
                                    </View>

                                }
                                {/*Seperater Line*/}
                                <View style={styles.viewSep}></View>
                                {/* Email*/}
                                {this.state.editable ?
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.ic_email} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.email} textStyle={styles.textHeading} />
                                        <TextInput
                                            ref='email'
                                            keyboardType="email-address"
                                            onChangeText={(text) => {
                                                this.setState({
                                                    newEmail: text.trim()
                                                });
                                            }}
                                            style={styles.textInput}
                                            value={this.state.newEmail}
                                            placeholder={strings.hint_email_id} />
                                    </View>
                                    :
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.ic_email} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.email} textStyle={styles.textHeading} />
                                        <TextRegular title={this.state.email} textStyle={styles.textData} />
                                    </View>

                                }
                                {/* Email*/}
                                {/* <View style={styles.viewSingleData}>
                                    <Image source={Images.ic_email} style={styles.iconLeft}></Image>
                                    <TextRegular title={strings.email} textStyle={styles.textHeading} />
                                    <TextRegular title={this.state.email} textStyle={styles.textData} />
                                </View> */}

                                {/*Seperater Line*/}
                                <View style={styles.viewSep}></View>

                                {/* Country Code*/}
                                {this.state.editable ?
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.phone} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.country_code_text} textStyle={styles.textHeading} />
                                        <TouchableOpacity onPress={() => NavigationService.navigate("CountrySelection", {
                                            onCountrySelect: this.onCountrySelect,
                                            selectedCountry: this.state.countrycode
                                        })} style={{ width: '100%' }}>
                                            <TextRegular title={this.state.newCountryCode} textStyle={styles.countryCodeInput}></TextRegular>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.phone} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.country_code_text} textStyle={styles.textHeading} />

                                        <TextRegular title={this.state.countrycode} textStyle={styles.textData} />
                                    </View>

                                }


                                {/*Seperater Line*/}
                                <View style={styles.viewSep}></View>

                                {/* Mobile*/}
                                {this.state.editable ?
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.phone} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.mobile_text} textStyle={styles.textHeading} />
                                        <TextInput
                                            maxLength={10}
                                            ref='mobileNo'
                                            keyboardType="numeric"
                                            onChangeText={(text) => {
                                                this.setState({
                                                    newmobile: text.trim()
                                                });
                                            }}
                                            style={styles.textInput}
                                            value={this.state.newmobile}
                                            placeholder={strings.phone} />
                                    </View>
                                    :
                                    <View style={styles.viewSingleData}>
                                        <Image source={Images.phone} style={styles.iconLeft}></Image>
                                        <TextRegular title={strings.mobile_text} textStyle={styles.textHeading} />

                                        <TextRegular title={this.state.mobile} textStyle={styles.textData} />
                                    </View>

                                }

                                {/*Seperater Line*/}
                                <View style={styles.viewSep}></View>

                                {
                                    this.state.editable ?
                                        /*Update Profile */
                                        <View style={styles.viewButton}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this._verifyAndUpdate();
                                                }}
                                                style={styles.touchOpacity}>
                                                <TextBold title={strings.profile} textStyle={styles.textButtonAdd} />
                                            </TouchableOpacity>
                                        </View>
                                        :
                                        /*Address Book */
                                        <View style={styles.viewButton}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this._addressBookClicked();
                                                }}
                                                style={styles.touchOpacity}>
                                                <TextBold title={strings.address_book} textStyle={styles.textButtonAdd} />
                                            </TouchableOpacity>
                                        </View>
                                }
                            </View>
                        </KeyboardAwareScrollView>
                    </View>
                    {this.renderProgressBar()}
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        userData: state.userData,
        internet: state.internet
    }
}

export default connect(mapStateToProps, { userDataAction })(Profile)







