import React, { Component } from 'react'
import { Platform, SafeAreaView, FlatList, Image, View, TouchableOpacity, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import { connect } from 'react-redux'
import styles from './styles'
import { Constants, strings, Images, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, } from '../custom/text'
import { getOrderList } from '../../services/APIService'
import { userDataAction, liveLocationAction } from '../../redux/actions'
import NavigationService from '../../services/NavigationServices'
import FirebaseDatabase from '../../firebase-database'

let orderDataList = [];
class Orders extends Component {

    static navigationOptions = {
        header: null
    };



    constructor(props) {
        super(props);
        this.state = {
            toogle: true,
            orderNodata: null,
            orderLoaderIsVisible: false,
            current_page: -10,
            last_page: -10,
            orderlist: [
                {
                    name: "Cake bake",
                    imageLogo: "",
                    address: "Lambarka Dalabka",
                    orderNumber: "1563354418",
                    date: "2019-07-17",
                    time: "09:06:58",
                    status: "Delivered",
                    amount: '370.00'
                },
                {
                    name: "Pizza Hut",
                    imageLogo: "",
                    address: "Sector 14 ",
                    orderNumber: "251181818",
                    date: "2019-07-18",
                    time: "09:06:58",
                    status: "Processing",
                    amount: '227.00'
                },
            ]
        }

        this.orderNotificationHandler = this.orderNotificationHandler.bind(this);

    }


    calculateStatus(id) {
        switch (parseInt(id)) {
            case Constants.ORDER_STATUS.processing_0:
                return strings.processing
            case Constants.ORDER_STATUS.processing_1:
                return strings.processing
            case Constants.ORDER_STATUS.preparing_2:
                return strings.preparing
            case Constants.ORDER_STATUS.preparing_3:
                return strings.preparing
            case Constants.ORDER_STATUS.on_the_way_4:
                return strings.ontheway
            case Constants.ORDER_STATUS.delivered_5:
                return strings.delievered
            case Constants.ORDER_STATUS.cancelled_6:
                return strings.cancelled
            case Constants.ORDER_STATUS.confirmed_7:
                return strings.confirmed
        }
    }

    orderClicked(order_number, index) {
        this.props.navigation.navigate('Order', {
            previousPage: 'Orders',
            order_number: order_number,
            index: index,
            updateStatus: this.updateStatus
        })
    }

    updateStatus = data => {
        const index = data.index
        if (orderDataList[index].status != data.status) {
            orderDataList[index].status = data.status;
        }
        this.setState({
            toogle: !this.state.toogle
        })

    };

    renderProgressBar() {
        if (this.state.orderLoaderIsVisible) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>
            )
        } else {
            return
        }
    }



    _orderlist = () => {
        return this.state.orderlist.map((item, index) =>
            (
                // Main Card 
                <TouchableOpacity key={index} style={styles.mainCard}>
                    {/* View For Image and Items*/}
                    <View style={{ flexDirection: 'row' }}>
                        {/* View For Image */}
                        <View style={styles.viewImg}>
                            <Image source={Images.no_restaurant} resizeMode='contain'
                                style={styles.imgLogo} ></Image>
                        </View>
                        {/* View For Items */}
                        <View style={styles.viewItems}>
                            <TextBold title={item.name} textStyle={styles.textItemName} />
                            <View style={styles.viewInputRow}>
                                <TextRegular title={item.address} textStyle={styles.textItems} />
                                <TextRegular title={strings.dateString} textStyle={styles.textItems} />
                            </View>
                            <View style={styles.viewInputRow}>
                                <TextRegular title={item.orderNumber} textStyle={styles.textItems} />
                                <TextRegular title={item.date + " " + item.time} textStyle={styles.textItems} />
                            </View>

                        </View>
                    </View>
                    {/* View For Buttons */}
                    <View style={{ flexDirection: 'row', backgroundColor: 'white', justifyContent: 'flex-end' }}>
                        {/* Button Amount */}
                        <View style={styles.buttonAmount} >
                            <TextBold title={'$' + item.amount} textStyle={styles.textAmountButton} />
                        </View>
                        {/* Button Status */}
                        <View style={styles.buttonStatus}   >
                            <TextBold title={item.status} textStyle={styles.textAmountProcessing} />
                        </View>

                    </View>
                </TouchableOpacity>
            )
        )
    }



    componentDidMount() {
        DeviceEventEmitter.addListener(Constants.EVENTS.orderNotification, this.orderNotificationHandler)
        if (this.props.userData) {
            const userData = this.props.userData
            userId = (userData.id) ? userData.id : ""
            this.setState({
                user_id: userId,
            })
            this.callInitGetOrderListApi(userId)
        }
    }

    componentWillUnmount() {
        console.log("UNMOUNT")
        DeviceEventEmitter.removeListener(Constants.EVENTS.orderNotification, this.orderNotificationHandler)
    }

    orderNotificationHandler() {
        console.log("EVENT")
        if (this.props.userData) {
            const userData = this.props.userData
            userId = (userData.id) ? userData.id : ""
            this.setState({
                user_id: userId,
            })
            console.log("CALL_API")
            this.callInitGetOrderListApi(userId)
        }
    }


    callInitGetOrderListApi = (user_id) => {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }

        console.log("INTERNET:" + this.props.internet)
        this.setState({
            orderLoaderIsVisible: true,
        })

        let param = {
            user_id: user_id,
            orderlimit: 10,
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.orderlist + "?page=1"

        getOrderList(url, param).then(res => {

            //console.log("INIT_Order:" + JSON.stringify(res))
            if (res && res.success && res.data) {
                this.setState({
                    current_page: res.data.current_page,
                    last_page: res.data.last_page,
                    orderLoaderIsVisible: false,
                })
            }

            if (res && res.success && res.data && res.data.data && res.data.data.length > 0) {
                console.log("INIT_Order:" + JSON.stringify(res.data.data))
                orderDataList = res.data.data;
                this.setState({
                    orderNodata: false,
                    orderLoaderIsVisible: false,
                })
            } else {
                this.setState({
                    orderNoData: true,
                    orderLoaderIsVisible: false,
                })
            }

            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                orderNoData: true,
                orderLoaderIsVisible: false,
            })
            setTimeout(() => {
                if (err) {
                    // err = JSON.parse(err)
                    if (err != '') {
                        alert(err);
                    }
                }
            }, 100);
        });
    }

    callGetOrderListApi() {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }

        this.setState({
            orderLoaderIsVisible: true,
        })

        let param = {
            user_id: this.state.user_id,
            orderlimit: 10,
        }

        let url = ''

        if (this.state.current_page != -10) {
            if (this.state.current_page && this.state.last_page && !(this.state.current_page === this.state.last_page)) {
                url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.orderlist + "?page=" + (parseInt(this.state.current_page) + 1)
            }
        }

        // let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantList + "/0" + "?page=1"


        if (url === '') {
            this.setState({
                orderLoaderIsVisible: false,
            })
            return
        }

        getOrderList(url, param).then(res => {
            console.log("REST_API_RES2:" + JSON.stringify(res))

            if (res && res.success && res.data) {
                this.setState({
                    current_page: res.data.current_page,
                    last_page: res.data.last_page,
                    orderLoaderIsVisible: false,
                })
            }
            if (res && res.success && res.data && res.data.data && res.data.data.length > 0) {
                console.log("REST_API_RES3:" + JSON.stringify(res))
                let newOrderDataList = res.data.data
                orderDataList = orderDataList.concat(newOrderDataList);
                this.setState(prevState => ({
                    orderLoaderIsVisible: false,
                }))
            } else {
                this.setState({
                    orderLoaderIsVisible: false,
                })
            }

            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                orderLoaderIsVisible: false,
            })
            setTimeout(() => {
                alert(JSON.stringify(err));
            }, 100);
        });

    }



    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.side_menu_my_orders} textStyle={styles.textTitle} />

                </View>
                {/* <View style={{ flexDirection: 'row',  width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginRight: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View> */}
            </View>

        )
    }
    onScrollHandler = () => {
        this.callGetOrderListApi();

    }

    renderMapImageView(item) {
        if (item && (item.status == Constants.ORDER_STATUS.on_the_way_4)) {
            if (this.props.liveLocationData && (this.props.liveLocationData.orderNumber != item.order_number)) {
                this.props.liveLocationAction(null)
            }
            return (
                <TouchableOpacity style={{ width: "20%", alignItems: 'center', justifyContent: 'center' }} onPress={() => NavigationService.navigate('TrackDriver', { order: item })}>
                    <View style={{ width: 30, height: 30, backgroundColor: Constants.color.primary, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={Images.ic_distance_location_white} style={{ width: 15, height: 15 }}
                            resizeMode='contain' />
                    </View>


                </TouchableOpacity>
            )
        } else {
            return null
        }

    }



    render() {
        let area = "";
        let cityName = "";
        let address = "";
        // if(item.restaurant && item.restaurant.area && item.restaurant.area.name && item.restaurant.area.name){
        //     area = item.restaurant.area.name
        // }

        // if(item.restaurant && item.restaurant.city && item.restaurant.city.name){
        //     cityName = item.restaurant.city.name
        // }

        // if(area){
        //     address = area
        // }
        // if(address != "" && cityName != ""){
        //     address = address + ", " + cityName
        // }else if(address == "" && cityName != ""){
        //     address = cityName
        // }

        return (
            <View style={{ flex: 1 }} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                {/* <FirebaseDatabase driverId={0} orderNumber={0} /> */}

                <View style={{ height: '100%', width: '100%' }}>
                    {this.renderToolbar()}

                    <View style={styles.loginForm}>
                        {this.state.orderNodata === null ?
                            null :
                            this.state.orderNodata ?
                                <View style={{ width: '100%', flex: 1, alignItems: 'center', justifyContent: 'center', paddingBottom: '15%' }}>
                                    <TextRegular title={strings.no_data_found} textStyle={{ fontSize: 20 }} />
                                </View>
                                :
                                <View style={{ width: '100%', }}>
                                    <FlatList
                                        // onRefresh={this.mRefresh}
                                        // refreshing={this.state.isRefreshing}
                                        data={orderDataList}
                                        extraData={this.state.toogle}
                                        keyExtractor={item => item.order_number}
                                        // ListHeaderComponent={this.renderHeader}
                                        onEndReached={this.onScrollHandler}
                                        onEndReachedThreshold={0.1}
                                        renderItem={({ item, index }) => (
                                            <TouchableOpacity style={styles.mainCard}
                                                onPress={() => this.orderClicked(item.order_number, index)} >
                                                {/* View For Image and Items*/}
                                                <View style={{ flexDirection: 'row' }}>
                                                    {/* View For Image */}
                                                    <View style={styles.viewImg}>
                                                        <Image source={item.restaurant.image && item.restaurant.image.location && item.restaurant.image.location != ''
                                                            ? { uri: item.restaurant.image.location } : Images.no_restaurant}
                                                            resizeMode='contain'
                                                            style={styles.imgLogo} ></Image>
                                                    </View>
                                                    {/* View For Items */}
                                                    <View style={styles.viewItems}>
                                                        <View style={styles.viewInputRow}>
                                                            <View style={{ width: "80%" }}>
                                                                <TextBold title={item.restaurant.name} textStyle={styles.textItemName} />
                                                            </View>

                                                            {this.renderMapImageView(item)}


                                                        </View>

                                                        <View style={styles.viewInputRow}>
                                                            <TextRegular title={
                                                                (item.restaurant && item.restaurant.area && item.restaurant.area.name)? item.restaurant.area.name:""+
                                                                (item.restaurant && item.restaurant.area && item.restaurant.area.name && item.restaurant.city && item.restaurant.city.name)?", ":""+
                                                                (item.restaurant && item.restaurant.city && item.restaurant.city.name)?item.restaurant.city.name:""
                                                            } textStyle={styles.textItems} />
                                                            <TextRegular title={strings.dateString} textStyle={styles.textItems} />
                                                        </View>
                                                        <View style={styles.viewInputRow}>
                                                            <TextRegular title={item.order_number} textStyle={styles.textItems} />
                                                            <TextRegular title={item.date + " " + item.time} textStyle={styles.textItems} />
                                                        </View>
                                                    </View>
                                                </View>
                                                {/* View For Buttons */}
                                                <View style={{ flexDirection: 'row', backgroundColor: 'white', justifyContent: 'flex-end' }}>
                                                    {/* Button Amount */}
                                                    <View style={styles.buttonAmount} >
                                                        <TextBold title={'$' + item.amount} textStyle={styles.textAmountButton} />
                                                    </View>
                                                    {/* Button Status */}
                                                    <View style={styles.buttonStatus}   >
                                                        <TextBold title={this.calculateStatus(item.status)} textStyle={styles.textAmountProcessing} />
                                                    </View>
                                                </View>
                                            </TouchableOpacity>)
                                        }
                                    />
                                </View>
                        }

                    </View>
                    {this.renderProgressBar()}
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        userData: state.userData,
        internet: state.internet,
        liveLocationAction: state.liveLocationAction,
        liveLocationData: state.liveLocationData,
    }
}

export default connect(mapStateToProps, { userDataAction, liveLocationAction })(Orders)









