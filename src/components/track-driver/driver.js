import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, DeviceEventEmitter, View, Dimensions, TouchableOpacity, Image, Text, SafeAreaView, StatusBar, Animated } from 'react-native';
import styles from './styles'
import FirebaseDatabase from '../../firebase-database'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import MapView, { Marker, PROVIDER_GOOGLE, AnimatedRegion, MapViewDirections, Permissions, Location } from 'react-native-maps'
import Geocoder from 'react-native-geocoding'
import { directionApi } from '../../services/APIService'
import PolylineDecoder from '@mapbox/polyline'

const latDelta = 0.015 * 5
const longDelta = 0.0121 * 5

class DriverComponent extends Component {

    constructor(props) {
        super(props)

        // const driver = this.props.driver ? this.props.driver : { location: { latitude: 0, longitude: 0 } }

        // const coordinate = new AnimatedRegion({
        //     latitudeDelta: latDelta,
        //     longitudeDelta: longDelta,
        //     latitude: driver.location.latitude,
        //     longitude: driver.location.longitude
        // })

        // this.state = {
        //     newLocation: this.props.driver && this.props.driver.location?this.props.driver.location:{ location: { latitude: 0, longitude: 0 }},
        //     oldLocation: this.props.driver && this.props.driver.oldLocation?this.props.driver.oldLocation:{ oldLocation: { latitude: 0, longitude: 0 }},
        //     oldBearing:0,
        //     newBearing:0,
        // }
    }

    // componentDidMount(){
    //     let oldBearing = this.state.newBearing
    //     let newBearing = this.getBearing(oldLocation.latitude,oldLocation.longitude,location.latitude,location.longitude)
    //     let newLocation = this.props.driver.location
    //     let oldLocation = this.props.driver.oldLocation

    //     this.setState({
    //         newLocation: newLocation,
    //         oldLocation: oldLocation,
    //         oldBearing:oldBearing,
    //         newBearing:newBearing,
    //     })
    // }

    

    // setBearingToMarker = (bearing) => {
    //     if(this.marker){
    //         this.marker.rotation = {bearing}
    //     }
    // }

    render() {
        // console.log("DRIVER_COMPONENT:" + JSON.stringify(this.props.driver))
        // let bearing = 0
        // if(this.props.driver && this.props.driver.location && this.props.driver.oldLocation){
        //     const{location,oldLocation} = this.props.driver
        //     bearing = this.getBearing(oldLocation.latitude,oldLocation.longitude,location.latitude,location.longitude)
            
        //     // this.setBearingToMarker(bearing)
        // }
        

        // JSONObject source = step.getJSONObject("start_location");
        // double lat1 = Double.parseDouble(source.getString("lat"));
        // double lng1 = Double.parseDouble(source.getString("lng"));

        // // destination
        // JSONObject destination = step.getJSONObject("end_location");
        // double lat2 = Double.parseDouble(destination.getString("lat"));
        // double lng2 = Double.parseDouble(destination.getString("lng"));

        // var dLon = (lng2 - lng1);
        // double y = Math.sin(dLon) * Math.cos(lat2);
        // double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
        // double brng = Math.toDegrees((Math.atan2(y, x)));
        // brng = (360 - ((brng + 360) % 360));
        return (
            <Marker.Animated
                coordinate={new AnimatedRegion({
                    latitudeDelta: latDelta,
                    longitudeDelta: longDelta,
                    latitude: this.props.driver.location.latitude,
                    longitude: this.props.driver.location.longitude
                })}
                anchor={{ x: 0.50, y: 0.50 }}
                // angle={this.props.driver.bearing}
                // rotation={this.props.driver.bearing}
                ref={marker => { this.marker = marker }}
                // flat={false}
                styles={{ width: 60, height: 60 }}>
                {/* <View style={{backgroundColor:'red',padding:5}}> */}
                <Animated.Image source={Images.ic_delivery_boy}  style={{ height: 60, width: 60 ,transform: [{ rotate: `${this.props.driver.bearing}deg` }]}} />

                    {/* </View> */}
            </Marker.Animated>
        );
    };
}

export default DriverComponent