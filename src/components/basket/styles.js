import { Platform, StyleSheet } from 'react-native';
import { Constants } from '../../utils'
import { Fonts } from '../../utils/fonts'


export default StyleSheet.create({
    contentContainer: {
    },
    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        // fontWeight: '500',
        color: Constants.color.white,
        marginLeft: 15,
    },
    safeStyle: {
        flex: 1,
        backgroundColor: 'transparent',
        alignItems: 'center',
        height: '100%',
        width: '100%',
    },
    container: {
        flex: 1,
        paddingTop: Constants.AppConstant.statusBarHeight,
    },
    infoContainer: {
        flex: 1,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textContainer: {
        flex: 1,
    },
    map: {
        // ...StyleSheet.absoluteFillObject,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    bottomSafeStyle: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%'
    },
    imageInfo: {
        height: 90,
        width: 90,
        margin: 15,
    },
    textInfo: {
        color: 'white',
        fontSize: 14,
    },
    textInfo2: {
        fontSize: 15,
    },
    textOrder: {
        // width: '30%',
        textAlign: 'right',
    },
    weekRow: {

        width: '100%',
        flexDirection: "row",

        // justifyContent: 'center',
        // alignItems: 'center',
    },
    weekRow2: {
        marginHorizontal: 20,
        flexDirection: "row",
        marginVertical: 5,
        padding: 5,
        alignItems: 'center',
    },
    viewHead: {
        backgroundColor: '#808080',
        width: '100%',
        height: 1,
        marginHorizontal: 35,
    },

    textHead: {
        color: 'black',
        margin: 20,
        marginTop: 25,
        marginBottom: 10,
    },

    textHead2: {
        color: 'black',
        marginHorizontal: 25,
    },
    textData: {
        color: '#808080',
        marginHorizontal: 22, marginVertical: 5,
        fontWeight: 'normal',
    },
    viewInput: {
        paddingTop: 5,
        flexDirection: "row",
        alignItems: 'flex-end',
        marginHorizontal: 22
    },
    inputLayout: {
        width: '80%',
        marginHorizontal: 10
    },
    textInput: {
        fontSize: 16,
        height: 40,
        width: '80%',
        fontFamily: Fonts.Regular,
        color: Constants.color.black
    },
    iconLeft: {
        height: 25,
        width: 25,
        margin: 5
    },
    ViewLogin: {
        height: '100%',
        alignItems: 'flex-end',
        width: '40%',
        justifyContent: 'center'
    },
    rowCheckbox: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 22,
        marginVertical: 5
    },
    columnCheckbox: {
        flexDirection: 'column',
        justifyContent: 'center',
        marginVertical: 15
    },
    textLoginNavigation: {
        color: Constants.color.white,
        paddingLeft: 10,
        paddingVertical: 3,
        fontSize: 14
    },
    apliedView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 25,
        padding:5,
        shadowOffset: { height: 1, width: 1 }, // IOS
        shadowOpacity: 0.5, // IOS
        shadowRadius: 1, //IOS
        borderRadius: 10,
        backgroundColor: 'white',
        elevation: 4, 
    },
    menuInfoImage: {
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 0,
        borderColor: Constants.color.primary,
    },

})

