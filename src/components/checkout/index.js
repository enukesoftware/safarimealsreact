import React, { Component } from 'react'
import { Platform, SafeAreaView, ActivityIndicator, Image, View, TouchableOpacity, AsyncStorage } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold,  } from '../custom/text'
import CheckBox from 'react-native-check-box'
import { addItemInBasketAction } from '../../redux/actions'
import { connect } from 'react-redux'
import { addOrderApi, getRestaurantDetail } from '../../services/APIService'
import { ScrollView } from 'react-native-gesture-handler';
let KEY_ORDER = null;


class Checkout extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedMethod: 'none',
            isCheckedCash: false,
            isCheckedEVC: false,
            navigateFrom: '',
            is_open: "1",
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        //console.log('addessJson:'+navigateFrom)
        let navigateFrom = this.props.navigation.getParam('NavigateFrom', '')
        this.setState({
            navigateFrom: navigateFrom
        })

        KEY_ORDER = navigation.getParam('KEY_ORDER', '');
        //console.log('Checkout_KEY_ORDER:' + JSON.stringify(KEY_ORDER))
        console.log('Checkout_KEY_ORDER:' + JSON.stringify(KEY_ORDER))


        if (this.props.userData) {
            const userData = this.props.userData
            userId = (userData.id) ? userData.id : ""
            this.setState({
                user_id: userId
            })
        }


    }
    // validation function
    _verifyAndPlaceOrder = () => {

        if (this.state.selectedMethod == "none") {
            alert("Please Select Payment Method")
            return
        }
        if (KEY_ORDER.order["user_payment_method"] != null && KEY_ORDER.order["user_payment_method"] != "") {
            this.callGetRestaurantDetailApi()
        }
    }

    callGetRestaurantDetailApi = () => {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }
        this.setState({
            isLoading: true
        })

        let param = {
            id: KEY_ORDER.order.restaurant_id
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantDetail

        getRestaurantDetail(url, param).then(res => {

            // console.log("REST_DETAIL_API_RES:" + JSON.stringify(res))
            // this.calladdOrderApi()
            if (res && res.success && res.data && res.data.is_open) {
                if (res.data.is_open == "1") {
                   this.calladdOrderApi()

                } else {
                    this.setState({
                        isLoading: false,
                    })
                }
            }
            else {
                this.setState({
                    isLoading: false,
                })
            }
        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });
    }

    calladdOrderApi() {
        if (!this.props.internet) {
            this.setState({
                isLoading: false,
            },()=>{
                alert(strings.message_lno_internet)
            })
            return
        }
        const param = {
            user_id: KEY_ORDER.user_id,
            order_id: KEY_ORDER.order_id,
            order: KEY_ORDER.order,
        }
    
        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.addorder

        addOrderApi(url, param).then(res => {
            if (res && res.success) {
                console.log("RS:" + JSON.stringify(res))
                this.props.addItemInBasketAction(null)
                AsyncStorage.removeItem(Constants.STORAGE_KEY.basketData);
                this.props.navigation.navigate('OrderConfirmation', {
                    NavigateFrom: 'Checkout',
                    KEY: res
                })
                // alert(res.message);
            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        console.log("RSERROR:" + JSON.stringify(res))
                        alert(res.error)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });

    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }

    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>
            )
        } else {
            return
        }

    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.title_checkout} textStyle={styles.textTitle} />

                </View>
                {/* <View style={{ flexDirection: 'row',  width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginRight: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View> */}
            </View>

        )
    }

    fetchMethod = (text) => {
        if (KEY_ORDER != null && KEY_ORDER != "") {
            KEY_ORDER.order["user_payment_method"] = text
        }
        switch (text) {
            case strings.COD:
                this.setState({
                    isCheckedCash: true,
                    isCheckedEVC: false,
                    selectedMethod: text
                })
                break;
            case strings.EVC:
                this.setState({
                    isCheckedCash: false,
                    isCheckedEVC: true,
                    selectedMethod: text
                })
        }

    }

    render() {
        return (
            <View style={{}} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                <View style={{ height: '100%', width: '100%' }}>
                    {this.renderToolbar()}
                    <View style={styles.loginForm}>
                        <ScrollView contentInsetAdjustmentBehavior="automatic"
                            keyboardShouldPersistTaps='always'
                            style={{}}>
                            <TextBold title={strings.payment_text} textStyle={styles.textInfo} />
                            {/*CheckBox COD */}
                            <CheckBox
                                style={{ padding: 10, margin: 15 }}
                                checkBoxColor={Constants.color.primary}
                                onClick={() => {
                                    this.fetchMethod("COD")
                                }}
                                isChecked={this.state.isCheckedCash}
                                rightText={strings.cod_text}
                                rightTextStyle={styles.rightTextStyle}
                            />

                            {/*CheckBox 2 */}
                            <CheckBox
                                style={{ padding: 10, margin: 15 }}
                                checkBoxColor={Constants.color.primary}
                                onClick={() => {
                                    this.fetchMethod("EVC")
                                }}
                                isChecked={this.state.isCheckedEVC}
                                rightText={strings.evc_plus}
                                rightTextStyle={styles.rightTextStyle}
                            />


                            {/*Button Place Order */}
                            <View style={styles.viewButton}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this._verifyAndPlaceOrder();
                                    }}
                                    style={styles.touchOpacity}>

                                    <TextBold title={
                                        this.state.navigateFrom == "Basket" ?
                                            strings.place_order : strings.modify_order} textStyle={styles.textButtonAdd} />
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                    {this.renderProgressBar()}
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    //console.log("BASKET_STATE:" + JSON.stringify(state.basketData))
    return {
        basketData: state.basketData,
        internet: state.internet
    }
}
export default connect(mapStateToProps, { addItemInBasketAction })(Checkout)







