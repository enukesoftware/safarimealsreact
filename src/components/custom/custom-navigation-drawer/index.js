import React, { Component } from 'react'
import { Platform, StyleSheet, View, SafeAreaView, Image, ActivityIndicator, Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../text'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import NavigationService from '../../../services/NavigationServices'
import { isLoginAction, userDataAction } from '../../../redux/actions'
import Counter from '../counter'
import { logoutApi } from '../../../services/APIService'



class CustomNavigationDrawer extends Component {

  constructor() {
    super()
  }

  state = {
    isLoading: false
  }

  onLogoutAlertClick = () => {
    this.setState({
      isLoading: true
    }, () => {
      this.callLogoutApi()
    })
  }

  showLogoutAlert = () => {
    Alert.alert(
      strings.logout_dialog_title,
      strings.logout_dialog_message,
      [
        //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
        {
          text: strings.cancel,
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: strings.ok,
          onPress: () => this.onLogoutAlertClick()
        },
      ],
      { cancelable: false },
    );
  }

  navigateToLogin = () => {
    (NavigationService.navigate('Login', { previousPage: 'Dashboard' }), NavigationService.closeDrawer())
  }


  onLogoutClick = () => {
    if (this.props.isLogin) {
      this.showLogoutAlert()
    } else {
      this.navigateToLogin()
    }
  }

  callLogoutApi() {
    if (!this.props.internet) {
      this.setState({
        isLoading: false
      }, () => {
        alert(strings.message_lno_internet)
      })

      return
    }
    const param = {
      user_id: this.props.userData.id,
    }


    let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.logout

    logoutApi(url, param).then(res => {
      // this.setState({
      //     isLoading: false,
      // })

      if (res && res.success) {
        AsyncStorage.clear();
        this.props.isLoginAction(false)
        this.props.userDataAction(null)
        AsyncStorage.setItem(Constants.STORAGE_KEY.isVisitedSteps, 'true');
        NavigationService.navigate("LocalityStack")

      } else {
        this.setState({
          isLoading: false,
        }, () => {
          if (res && res.error) {
            alert(res.error)
          }
        })
      }

    }).catch(err => {
      this.setState({
        isLoading: false,
      })
      setTimeout(() => {
        if (err) {
          alert(JSON.stringify(err));
        }
      }, 100);
    });

  }


  renderTableBookingView() {
    if (!this.props.isLogin) {
      return
    }
    return (
      <TouchableOpacity style={styles.bottomItemsView}
        onPress={() => (NavigationService.navigate('TableBookingHistory'), NavigationService.closeDrawer())}>
        <Image source={Images.ic_booking_history} style={styles.bottomsItemsImage}></Image>
        <TextRegular title={strings.side_menu_table_booking} textStyle={styles.bottomsItemsText} />
      </TouchableOpacity>
    )
  }

  renderOrdersView() {
    if (!this.props.isLogin) {
      return
    }

    return (
      <TouchableOpacity style={styles.bottomItemsView}
        onPress={() => (NavigationService.navigate('Orders'), NavigationService.closeDrawer())}>
        <Image source={Images.ic_orders} style={styles.bottomsItemsImage}></Image>
        <TextRegular title={strings.side_menu_my_orders} textStyle={styles.bottomsItemsText} />
      </TouchableOpacity>
    )
  }

  renderMyProfileView() {
    if (!this.props.isLogin) {
      return
    }
    return (
      <TouchableOpacity style={styles.bottomItemsView}
        onPress={() => (NavigationService.navigate('Profile'), NavigationService.closeDrawer())}>
        <Image source={Images.ic_my_profile} style={styles.bottomsItemsImage}></Image>
        <TextRegular title={strings.side_menu_my_profile} textStyle={styles.bottomsItemsText} />
      </TouchableOpacity>
    )
  }

  renderChangePasswordView() {
    if (!this.props.isLogin) {
      return
    }
    return (
      <TouchableOpacity style={styles.bottomItemsView}
        onPress={() => (NavigationService.navigate('ChangePassword'), NavigationService.closeDrawer())}>
        <Image source={Images.ic_change_password} style={styles.bottomsItemsImage}></Image>
        <TextRegular title={strings.side_menu_change_paasword} textStyle={styles.bottomsItemsText} />
      </TouchableOpacity>
    )
  }

  renderProgressBar() {
    if (this.state.isLoading) {
      return (
        <View style={GlobalStyle.activityIndicatorView}>
          <View style={GlobalStyle.activityIndicatorWrapper}>
            <ActivityIndicator
              size={"large"}
              color={Constants.color.primary}
              animating={true} />
          </View>
        </View>

      )
    } else {
      return
    }

  }


  render() {

    let userImage = Images.ic_avatar
    let userName = strings.user_name_guest
    let userEmail = ""
    let userMobile = ""

    if (this.props.userData) {
      const userData = this.props.userData
      // console.log("USER_DATA_DRAWER:" + JSON.stringify(this.props.userData))

      userImage = (userData.profile_image && userData.profile_image != "") ? { uri: userData.profile_image } : userImage

      if (userData.first_name) {
        userName = userData.first_name
        if (userData.last_name) {
          userName = userName + ' ' + userData.last_name
        }
      }
      userEmail = (userData.email) ? userData.email : ""
      userMobile = (userData.contact_number) ? userData.contact_number : ""

    }



    return (
      // <View>
      //     <SafeAreaView></SafeAreaView>
      //     style={
      //         styles.container
      //     }>
      // </View>


      <View style={
        styles.container
      }>
        <SafeAreaView ></SafeAreaView>

        <ScrollView style={{ backgroundColor: 'white' }}>
          {/*---------- Header View ----------*/}
          <View
            style={styles.headerView}>
            {/* <TouchableOpacity>
                            <Image source={Images.ic_settings}
                                style={styles.settingsImage} ></Image>
                        </TouchableOpacity> */}

            <Image source={userImage}
              style={styles.userImage}></Image>
            <TextBold title={userName} textStyle={styles.userName} />

            {
              (userEmail != "") ?

                <View style={styles.emailView}>
                  <Image source={Images.ic_envelope} style={styles.emailImage}></Image>
                  <TextRegular title={userEmail} textStyle={styles.emailText} />
                </View>
                :
                <View />
            }
            {
              (userMobile != "") ?
                <View style={styles.emailView}>
                  <Image source={Images.ic_mobile} style={styles.emailImage}></Image>
                  <TextRegular title={userMobile} textStyle={styles.emailText} />
                </View>
                :
                <View />
            }
          </View>

          {/*---------- Bottom View ----------*/}
          <View
            style={styles.bottomView}>
            {this.renderTableBookingView()}
            <TouchableOpacity style={styles.bottomItemsView}
              onPress={() => (NavigationService.navigate('Basket'), NavigationService.closeDrawer())}>
              <View style={{ width: '80%', flexDirection: 'row' }}>
                <Image source={Images.ic_cart_red} style={styles.bottomsItemsImage}></Image>
                <TextRegular title={strings.side_menu_basket} textStyle={styles.bottomsItemsText} />
              </View>
              <View style={{ width: '20%', alignItems: "center", justifyContent: 'center' }}>
                <Counter value={(this.props.basketData) ? this.props.basketData.totalQuantity : 0} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomItemsView}
              onPress={() => (NavigationService.navigate('SelectLocality', { NavigateFrom: 'Dashboard' }), NavigationService.closeDrawer())}>
              <Image source={Images.ic_address} style={styles.bottomsItemsImage}></Image>
              <TextRegular title={strings.side_menu_address} textStyle={styles.bottomsItemsText} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomItemsView}
              onPress={() => (NavigationService.closeDrawer())}>
              <Image source={Images.ic_restaurants} style={styles.bottomsItemsImage}></Image>
              <TextRegular title={strings.side_menu_restaurants} textStyle={styles.bottomsItemsText} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomItemsView}
              onPress={() => (NavigationService.navigate('Deals'), NavigationService.closeDrawer())}>
              <View style={{ width: '80%', flexDirection: 'row' }}>
                <Image source={Images.ic_deals} style={styles.bottomsItemsImage}></Image>
                <TextRegular title={strings.side_menu_deals} textStyle={styles.bottomsItemsText} />
              </View>
              <View style={{ width: '20%', alignItems: "center", justifyContent: 'center' }}>
                <Counter value={0} />
              </View>

            </TouchableOpacity>
            {this.renderOrdersView()}
            {this.renderMyProfileView()}
            {this.renderChangePasswordView()}
            <TouchableOpacity style={styles.bottomItemsView}
              onPress={() => (NavigationService.navigate('AboutUs'), NavigationService.closeDrawer())}>
              <Image source={Images.ic_about_us} style={styles.bottomsItemsImage}></Image>
              <TextRegular title={strings.side_menu_about_us} textStyle={styles.bottomsItemsText} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomItemsView}
              onPress={() => (NavigationService.navigate('ContactUs'), NavigationService.closeDrawer())}>
              <Image source={Images.ic_contact_us} style={styles.bottomsItemsImage}></Image>
              <TextRegular title={strings.side_menu_contact_us} textStyle={styles.bottomsItemsText} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomItemsView}
              onPress={() => this.onLogoutClick()}>
              <Image source={Images.ic_logout} style={styles.bottomsItemsImage}></Image>
              <TextRegular title={this.props.isLogin ? strings.side_menu_logout : strings.side_menu_signIn} textStyle={styles.bottomsItemsText} />
            </TouchableOpacity>
          </View>

        </ScrollView>
        {this.renderProgressBar()}
      </View>


    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constants.color.primary
  },
  headerView: {
    // height: '30%',
    paddingHorizontal: 10,
    paddingBottom: 10,
    backgroundColor: Constants.color.primary
  },
  settingsImage: {
    width: 24,
    height: 24,
    alignSelf: 'flex-end'
  },
  userImage: {
    height: 80,
    width: 80,
    alignSelf: 'center',
    marginTop: 20,
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 80 / 2,
  },
  userName: {
    color: 'white',
    marginTop: 10,
    textAlign: 'center',
  },
  emailView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5
  },
  emailImage: {
    height: 15,
    width: 15,

  },
  emailText: {
    color: 'white',
    marginLeft: 5,
  },
  bottomView: {
    // height: '70%',
    padding: 10,
    backgroundColor: 'white'
  },
  bottomItemsView: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10
  },
  bottomsItemsImage: {
    height: 25,
    width: 25,

  },
  bottomsItemsText: {
    color: Constants.color.fontBlack,
    marginLeft: 20,
    fontSize: Constants.fontSize.NormalX
  },
})

function mapStateToProps(state) {
  // console.log("DRAWER_STATE:" + JSON.stringify(state))
  return {
    isLogin: state.isLogin,
    userData: state.userData,
    internet: state.internet,
    basketData: state.basketData
    // location: state.location,
    // address: state.location.addressToDisplay,
    // city: state.location.city,
    // isCloseVisible: state.location.addressToDisplay === strings.hint_location ? false : true,
  }
}

export default connect(mapStateToProps, { isLoginAction, userDataAction })(CustomNavigationDrawer)