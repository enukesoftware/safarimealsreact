import React, { Component } from 'react'
import { Platform, TextInput, SafeAreaView, TouchableHighlight, Image, View, TouchableOpacity, ActivityIndicator, } from 'react-native';
import styles from './styles'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextInputLayout } from 'rn-textinputlayout';
import { ScrollView } from 'react-native-gesture-handler';
import { addressApi } from '../../services/APIService'
import { connect } from 'react-redux'
import { userDataAction } from '../../redux/actions'
import Geocoder from 'react-native-geocoding'


class AddAddress extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            houseno: "",
            street: "",
            area: "",
            city: "",
            state: "",
            user_id: '',
        }
        //function for validating and adding address
    }
    componentDidMount() {
        if (this.props.userData) {
            const userData = this.props.userData
            userId = (userData.id) ? userData.id : ""
            this.setState({
                user_id: userId,
            })
        }
    }

    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>
            )
        } else {
            return
        }
    }
    _verifyAndAddAddress = () => {
        if (this.state.houseno == "") {
            alert(strings.house_no_error)
            return
        }
        if (this.state.street == "") {
            alert(strings.street_error)
            return
        }
        if (this.state.area == "") {
            alert(strings.area_error)
            return
        }
        if (this.state.city == "") {
            alert(strings.city_error)
            return
        }
        if (this.state.state == "") {
            alert(strings.state_error)
            return
        }
        //here u add address

        // this.callAddAddressApi()
        this.getLocationFromAddress(this.state.houseno + ' ,' + this.state.street + ' ,' + this.state.area + ' ,' + this.state.city + ' ,' + this.state.state)
    }
    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }
    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.add_address} textStyle={styles.textTitle} />

                </View>
            </View>

        )
    }

    getLocationFromAddress = (address) => {
        this.setState({
            isLoading: true,
        })
        Geocoder.getFromLocation(address)
            .then(json => {
                console.log("ADDRESS:", JSON.stringify(json))

                if(json.results && json.results.length>0 && json.results[0].geometry && json.results[0].geometry.location){
                    this.location = json.results[0].geometry.location;
                    this.callAddAddressApi()
                }else{
                    this.setState({
                        isLoading: false,
                    })
                    setTimeout(() => {
                        alert(strings.address_not_found);
                    }, 100);
                }


            })
            .catch(error => {
                console.log("ERROR:" + JSON.stringify(error))
                this.setState({
                    isLoading: false,
                })
                setTimeout(() => {
                    alert(strings.address_not_found);
                }, 100);
            });
    }

    callAddAddressApi() {
        if(!this.props.internet){
            alert(strings.message_lno_internet)
            return
        }
        const param = {
            user_id: this.state.user_id,
            house_no: this.state.houseno,
            street: this.state.street,
            area_name: this.state.area,
            city: this.state.city,
            state_id: this.state.state,
            type: 'add',
            latitude:this.location.lat,
            longitude:this.location.lng,
        }

        // console.log("ADDRESS:"+JSON.stringify(param))
        
        // return

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.address

        addressApi(url, param).then(res => {
            // this.setState({
            //     isLoading: false,
            // })

            if (res && res.success) {
                console.log("ADD_ADDRESS_RESPONSE:" + JSON.stringify(res))
                this.setState({
                    isLoading: false,
                    editable: false,
                })
                /*   this.setState({
                      mobile:this.props.userData.contact_number
                  }) */
                alert(res.message);
                this.onBackClick();

            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });

    }
    render() {

        return (
            <View style={{ flex: 1, backgroundColor: Constants.color.white }} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>


                {this.renderToolbar()}

                <View style={styles.loginForm}>
                    <ScrollView contentInsetAdjustmentBehavior="automatic"
                        keyboardShouldPersistTaps='always'
                        style={{}}>

                        {/* house number*/}

                        <View style={styles.viewInput}>
                            <Image source={Images.ic_house} style={styles.iconLeft}></Image>
                            <TextInputLayout
                                style={styles.inputLayout}
                                focusColor={Constants.color.black}>
                                <TextInput
                                    onChangeText={(text) => {
                                        this.setState({
                                            houseno: text.trim()
                                        });
                                    }}
                                    style={styles.textInput}
                                    placeholder={strings.hint_house_no} />

                            </TextInputLayout>
                        </View>



                        {/* Street*/}
                        <View style={styles.viewInput}>
                            <Image source={Images.ic_flag} style={styles.iconLeft}></Image>
                            <TextInputLayout
                                style={styles.inputLayout}
                                focusColor={Constants.color.black}>
                                <TextInput
                                    onChangeText={(text) => {
                                        this.setState({
                                            street: text.trim()
                                        });
                                    }}
                                    style={styles.textInput}
                                    placeholder={strings.hint_street} />
                            </TextInputLayout>
                        </View>

                        {/* area*/}
                        <View style={styles.viewInput}>
                            <Image source={Images.ic_address} style={styles.iconLeft}></Image>
                            <TextInputLayout
                                style={styles.inputLayout}
                                focusColor={Constants.color.black}>
                                <TextInput
                                    onChangeText={(text) => {
                                        this.setState({
                                            area: text.trim()
                                        });
                                    }}
                                    style={styles.textInput}
                                    placeholder={strings.hint_area} />
                            </TextInputLayout>
                        </View>
                        {/* city*/}
                        <View style={styles.viewInput}>
                            <Image source={Images.ic_location} style={styles.iconLeft}></Image>
                            <TextInputLayout
                                style={styles.inputLayout}
                                focusColor={Constants.color.black}>
                                <TextInput
                                    onChangeText={(text) => {
                                        this.setState({
                                            city: text.trim()
                                        });
                                    }}
                                    style={styles.textInput}
                                    placeholder={strings.hint_city} />
                            </TextInputLayout>
                        </View>

                        {/* state*/}
                        <View style={styles.viewInput}>
                            <Image source={Images.ic_state} style={styles.iconLeft}></Image>
                            <TextInputLayout
                                style={styles.inputLayout}
                                focusColor={Constants.color.black}>
                                <TextInput
                                    onChangeText={(text) => {
                                        this.setState({
                                            state: text.trim()
                                        });
                                    }}
                                    style={styles.textInput}
                                    placeholder={strings.hint_state} />
                            </TextInputLayout>
                        </View>

                    </ScrollView>
                </View>

                <TouchableOpacity
                    onPress={() => {
                        this._verifyAndAddAddress();
                    }}
                    style={styles.touchOpacity}>
                    <TextRegular textStyle={styles.textButtonAdd} title={strings.add_address}></TextRegular>
                </TouchableOpacity>
                <SafeAreaView style={{}}></SafeAreaView>
                {this.renderProgressBar()}
            </View>

        );

    }

}

function mapStateToProps(state) {
    return {
        userData: state.userData,
        internet:state.internet
    }
}

export default connect(mapStateToProps, { userDataAction })(AddAddress)
//export default AddAddress







