import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, StyleSheet, View, Image, ImageBackground, Animated, Dimensions, StatusBar, ActivityIndicator } from 'react-native';
import styles from './styles'
import { SafeAreaView } from 'react-navigation';
import MenuSection from '../ListItem/MenuSection'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import NavigationService from '../../services/NavigationServices'
import { getRestaurantDetail } from '../../services/APIService'
import Counter from '../custom/counter'
import { createStackNavigator, createDrawerNavigator, NavigationEvents } from "react-navigation";


let collapseHeight = 200
class RestaurantMenu extends Component {

    static navigationOptions = {
        header: null,
    }

    state = {
        resId: '',
        restDetail: null,
        menuList: [],
        is_open:"1",
        isCartVisible: false,
        scrollY: new Animated.Value(0.01),
        isLoading: false,
        noDataFound: false,

    }

    constructor() {
        super()
        console.log("const")

    }

    componentDidMount() {
        const { navigation } = this.props;
        const restDetail = navigation.getParam('item', '');

        this.setState({
            resId: restDetail.id,
        }, () => {
            this.callGetRestaurantDetailApi()
        })
    }

    updateViewByBasketData = () => {
        console.log("updateViewByBasketData1")
        if (this.state.menuList && this.state.menuList.length > 0) {
            console.log("updateViewByBasketData2")
            let menus = []
            this.state.menuList.forEach(menu => {
                let menuItem = menu
                let products = []
                if (menu.products && menu.products.length > 0) {
                    menu.products.forEach(product => {
                        let productItem = product
                        let price = 0
                        let unitPrice = 0
                        let quantity = 0
                        if (product.cost) {
                            price = parseInt(product.cost)
                            if (this.props.basketData
                                && this.props.basketData.products
                                && this.props.basketData.products.length > 0) {
                                this.props.basketData.products.forEach(basketProduct => {
                                    if (basketProduct.id === product.id) {
                                        quantity = basketProduct.quantity
                                        price = basketProduct.productPrice
                                        unitPrice = basketProduct.unitPrice
                                        console.log("FOR_EACH:" + price + "   " + quantity)
                                    }
                                })
                            }
                        }
                        productItem = {
                            ...productItem,
                            quantity: quantity,
                            productPrice: price,
                            unitPrice: unitPrice,
                        }
                        products = [...products, ...[productItem]]
                    });
                }
                menuItem = {
                    ...menuItem,
                    products: products
                }
                menus = [...menus, ...[menuItem]]

            });

            this.setState({
                noDataFound: false,
                menuList: [],
                isLoading: false,
            }, () => {
                this.setState({
                    menuList: menus,
                })
            })
        }
    }

    callGetRestaurantDetailApi = () => {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }

        this.setState({
            isLoading: true
        })

        let param = {
            id: this.state.resId,
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantDetail

        getRestaurantDetail(url, param).then(res => {

            // console.log("REST_DETAIL_API_RES:" + JSON.stringify(res))

            if (res && res.success && res.data) {
                this.setState({
                    restDetail: res.data
                }, () => {
                    if (res && res.success && res.data && res.data && res.data.menus.length > 0) {
                        let menus = []
                        res.data.menus.forEach(menu => {
                            let menuItem = menu
                            let products = []
                            if (menu.products && menu.products.length > 0) {
                                menu.products.forEach(product => {
                                    let productItem = product
                                    let price = 0
                                    let unitPrice = 0
                                    let quantity = 0
                                    if (product.cost) {
                                        price = parseInt(product.cost)
                                        unitPrice = parseInt(product.cost)
                                        if (this.props.basketData
                                            && this.props.basketData.products
                                            && this.props.basketData.products.length > 0) {
                                            this.props.basketData.products.forEach(basketProduct => {
                                                if (basketProduct.id === product.id) {
                                                    quantity = basketProduct.quantity
                                                    price = basketProduct.productPrice
                                                    unitPrice = basketProduct.unitPrice

                                                    // console.log("FOR_EACH:"+quantity+ "   "+price)
                                                }
                                            })
                                        }
                                    }
                                    productItem = {
                                        ...productItem,
                                        quantity: quantity,
                                        productPrice: price,
                                        unitPrice: unitPrice
                                    }
                                    products = [...products, ...[productItem]]
                                });
                            }
                            menuItem = {
                                ...menuItem,
                                products: products
                            }
                            menus = [...menus, ...[menuItem]]

                        });

                        this.setState({
                            noDataFound: false,
                            menuList: menus,
                            isLoading: false,
                        })
                    } else {
                        this.setState({
                            noDataFound: true,
                            isLoading: false,
                        })
                    }
                })
            } else {
                this.setState({
                    noDataFound: true,
                    isLoading: false,
                })
            }



            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                noDataFound: true,
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    // err = JSON.parse(err)
                    alert(err);
                }
            }, 100);
        });
    }

    onBackClick = () => {
        NavigationService.goBack()
    }
    renderItemUnavailableView() {
        if (this.state.is_open != '1') {
            return (
                <View style={{
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                }}>
                    <View style={{
                        // position: 'absolute',
                        width: '100%',
                        height: '100%',
                        backgroundColor: 'gray',
                        opacity: 0.8,
                        borderRadius: 5,
                        // shadowOffset: { width: 1, height: 1 },
                        // shadowOpacity: 0.2,
                        // elevation: 5
                    }}>
                    </View>

                    <TextBold title={strings.currently_unavailable} textStyle={{ fontSize: Constants.fontSize.NormalXX, alignSelf: 'center', position: 'absolute', top: '40%', color: Constants.color.black }}></TextBold>

                </View>
            )
        } else {
            return (
                <View></View>
            )
        }


    }
    renderHeaderBottomView = (restDetail) => {
        let rating = '0'
        let deliveryTime = '--'
        let deliveryCharges = '--'

        if (restDetail) {
            if (restDetail.rating) {
                rating = restDetail.rating
            }

            if (restDetail.delivery_time) {
                deliveryTime = restDetail.delivery_time
            }

            if (restDetail.delivery_charge) {
                deliveryCharges = restDetail.delivery_charge
            }
        }


        return (
            // <View style={{ flex: 1, height: '100%' }}>
            /* Bottom View */
            <View style={styles.bottomView}>
                <View style={{ flex: 4, alignItems: 'center' }}>
                    <View style={[styles.bottomViewBlock]}>
                        <Image source={Images.ic_star} style={styles.blockImage}></Image>
                        <TextRegular title={rating} textStyle={[styles.blockValue]} />
                    </View>
                    <TextRegular title={strings.rating} textStyle={styles.blockLabel} />
                </View>

                <View style={styles.blockDivider} />
                <View style={{ flex: 4, alignItems: 'center' }}>
                    <View style={[styles.bottomViewBlock]}>
                        <Image source={Images.ic_clock} style={styles.blockImage}></Image>
                        <TextRegular title={deliveryTime + ' ' + strings.minutes} textStyle={[styles.blockValue]} />
                    </View>
                    <TextRegular title={strings.delivery_time} textStyle={styles.blockLabel} />
                </View>

                <View style={styles.blockDivider} />
                <View style={{ flex: 4, alignItems: 'center' }}>
                    <View style={[styles.bottomViewBlock]}>
                        <Image source={Images.ic_dollar} style={styles.blockImage}></Image>
                        <TextRegular title={deliveryCharges} textStyle={[styles.blockValue]} />
                    </View>
                    <TextRegular title={strings.delivery_charges} textStyle={styles.blockLabel} />
                </View>

            </View>

            // </View>
        )
    }

    renderMenuList = () => {
        if (this.state.noDataFound) {
            return (
                <View style={{ width: '100%', flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: '50%' }}>
                    <TextRegular title={strings.no_data_found} textStyle={{ fontSize: 20 }} />
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1, height: '100%', marginTop: 10 }}>
                    {this.state.menuList.map((item, key) => (
                        <MenuSection
                            key={key}
                            value={item}
                            restDetail={this.state.restDetail}
                        />
                        // <TextBold title={value.menu_name}></TextBold>
                    ))}

                    {/* <View style={{backgroundColor:'red',height:600,width:'100%'}}>
    
                    </View> */}
                    {/* <ScrollView>
                        {this.state.menu.map((item, key) => (
                            <MenuSection
                                key={key}
                                value={item}
                            />
                        ))}
                    </ScrollView> */}

                    <View style={{ margin: (this.props.basketData && this.props.basketData.products && this.props.basketData.products.length > 0) ? 25 : 10 }} />
                
                  
                </View>
            )
        }

    }

    renderBasketView = () => {
        if (this.props.basketData && this.props.basketData.products && this.props.basketData.products.length > 0) {
            return (

                <View style={styles.basketViewBlock}>
                    <View style={styles.basketView}>
                        <View style={styles.basketViewLeft}>
                            <TextBold title={this.props.basketData.totalQuantity + ' ' + strings.Item} textStyle={{ color: Constants.color.fontWhite, fontSize: Constants.fontSize.SmallXXX }} />
                            <View style={{ width: 1.2, height: '100%', backgroundColor: Constants.color.white, marginHorizontal: 10 }} />
                            <TextBold title={Constants.currency.dollar + ' ' + this.props.basketData.totalPrice} textStyle={{ color: Constants.color.fontWhite, fontSize: Constants.fontSize.SmallXXX }} />
                        </View>

                        <View style={styles.basketViewRight}>
                            <TouchableOpacity style={styles.viewBasketTouchable} onPress={() => NavigationService.navigate('Basket')}>
                                <TextBold title={strings.view_basket} textStyle={styles.viewBasketText} />
                            </TouchableOpacity>
                        </View>

                    </View>

                    <SafeAreaView />
                </View>
            );
        } else {
            return (
                <View />
            );
        }
    }

    renderProgressBar() {
        if (this.state.isLoading) {
            console.log("LOADER:::::")
            return (
                <View style={styles.activityIndicatorView}>
                    <View style={styles.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={this.state.loading} />
                    </View>
                </View>

            )
        } else {
            return (
                <View />
            )
        }

    }

    render() {

        console.log('restoC', JSON.stringify(this.props.basketData))

        let translateY = this.state.scrollY.interpolate({
            inputRange: [0, collapseHeight],
            outputRange: [0, -((collapseHeight) - (Constants.AppConstant.statusBarHeight + (Platform.OS === Constants.PLATFORM.ios) ? 44 : 56))],
            extrapolate: 'clamp',
        });

        let scale = this.state.scrollY.interpolate({
            inputRange: [0, collapseHeight],
            outputRange: [1, 0.5],
            extrapolate: 'clamp',
        });

        let textTranslateY = this.state.scrollY.interpolate({
            inputRange: [0, 90],
            outputRange: [0, 20],
            extrapolate: 'clamp',
        });

        let animatBackgroundColor = this.state.scrollY.interpolate({
            inputRange: [0, collapseHeight],
            outputRange: ['transparent', Constants.color.primary],
            extrapolate: 'clamp'
        });

        let animateImageHeight = this.state.scrollY.interpolate({
            inputRange: [0, collapseHeight],
            outputRange: [0, -((collapseHeight) - (Constants.AppConstant.statusBarHeight + (Platform.OS === Constants.PLATFORM.ios) ? 44 : 56))],
            extrapolate: 'clamp',
        });

        let animateLinearOpacity = this.state.scrollY.interpolate({
            inputRange: [0, Constants.AppConstant.statusBarHeight + (Platform.OS === Constants.PLATFORM.ios) ? 44 : 56],
            outputRange: [0, 0.5],
            extrapolate: 'clamp',
        })

        let animateEditBtn = this.state.scrollY.interpolate({
            inputRange: [0, (collapseHeight) - 25],
            outputRange: [0, -(((collapseHeight) - 25) - (Constants.AppConstant.statusBarHeight + (Platform.OS === Constants.PLATFORM.ios) ? 44 : 56))],
            extrapolate: 'clamp',
        });

        let animateEditScale = this.state.scrollY.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolate: 'clamp',
        })


        // const { navigation } = this.props;
        // const restDetail = navigation.getParam('item', '');
        const restDetail = this.state.restDetail

        let restImage = Images.no_restaurant;
        let restName = ""
        if (restDetail && restDetail.image && restDetail.image.location && restDetail.image.location != '') {
            restImage = { uri: restDetail.image.location }
        }

        if (restDetail && restDetail.name) {
            restName = restDetail.name
        }



        return (
            <View style={styles.container}>
                {/* <SafeAreaView>
                </SafeAreaView> */}

                <NavigationEvents
                    onWillFocus={() => {
                        this.updateViewByBasketData()
                        // this.setState({
                        //     // resId: restDetail.id,
                        //     isLoading: true
                        // }, () => {
                        //     this.callGetRestaurantDetailApi()
                        // })
                    }}
                />

                <View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0
                }}>
                    {this.renderProgressBar()}
                </View>

                <Animated.ScrollView
                    // scrollEventThrottle={20}
                    onScroll={Animated.event([
                        { nativeEvent: { contentOffset: { y: this.state.scrollY } } },
                    ])}
                    contentContainerStyle={{ paddingTop: Platform.OS === Constants.PLATFORM.ios ? ((collapseHeight)) : ((collapseHeight)) }}
                    collapsable={true}
                    bounces={false}
                    style={{ flex: 1 }}>

                    
                    {this.renderHeaderBottomView(restDetail)}
                    {this.renderMenuList()}
                </Animated.ScrollView>

                { //Unavailable
                    this.renderItemUnavailableView()}

                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    height: Platform.OS === Constants.PLATFORM.ios ? collapseHeight : collapseHeight,
                    backgroundColor: 'transparent',
                    // alignItems: 'center',
                    // justifyContent: 'flex-end',
                    // paddingBottom: 30,
                    transform: [{ translateY }],
                }}>

                    <View style={{ flex: 1 }}>
                        <ImageBackground source={Images.bg} style={{ width: '100%', flex: 1 }}>
                            <View style={styles.headerShadow} />

                            <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'flex-end', paddingBottom: 15 }}>
                                <Image source={restImage} style={styles.restaurantImage}></Image>
                                <TextBold title={restName} textStyle={styles.restName} />
                            </View>
                        </ImageBackground>

                    </View>
                </Animated.View>

                <Animated.View style={{
                    flexDirection: 'row', paddingTop: Platform.OS === Constants.PLATFORM.ios ? Constants.AppConstant.statusBarHeight : 0, top: 0, left: 0, right: 0, position: 'absolute', height: (Platform.OS === Constants.PLATFORM.ios) ?
                        Constants.TOOLBAR_HEIGHT.ios + Constants.AppConstant.statusBarHeight : Constants.TOOLBAR_HEIGHT.android, backgroundColor: animatBackgroundColor
                }}>
                    <Animated.View style={{ height: '100%', width: Platform.OS === Constants.PLATFORM.ios ? '25%' : '15%', paddingLeft: 15, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <TouchableOpacity style={{ marginTop: Platform.OS === Constants.PLATFORM.ios ? 10 : 0, }} underlayColor='transparent'
                            onPress={() => this.onBackClick()}>
                            <Image style={{ height: Platform.OS === Constants.PLATFORM.ios ? 20 : 35, width: Platform.OS === Constants.PLATFORM.ios ? 20 : 35 }} resizeMode='contain' source={Platform.OS === Constants.PLATFORM.ios ? Images.ic_back_ios : Images.ic_back_android} />
                        </TouchableOpacity>
                    </Animated.View>

                    <Animated.View style={{ height: '100%', width: Platform.OS === Constants.PLATFORM.ios ? '50%' : '60%', justifyContent: 'center', alignItems: Platform.OS === Constants.PLATFORM.ios ? 'center' : 'flex-start' }}>
                        <TextRegular
                            title={strings.restaurant_info}
                            ellipsizeMode={'tail'} numberOfLines={1}
                            textStyle={{ fontSize: 20, color: 'white', fontWeight: '500', marginTop: Platform.OS === Constants.PLATFORM.ios ? 10 : 0 }} />
                    </Animated.View>
                    <Animated.View style={{ height: '100%', width: '25%', flexDirection: 'row', marginTop: Platform.OS === Constants.PLATFORM.ios ? 5 : 0, justifyContent: 'flex-end', alignItems: 'center' }}>
                        <TouchableOpacity style={{ paddingLeft: 10, paddingRight: 15 }} onPress={() => NavigationService.navigate('Basket')}>
                            <Image style={{ height: 25, width: 25 }} resizeMode='contain' source={Images.ic_cart_white} />
                            <Counter value={this.props.basketData ? this.props.basketData.totalQuantity : 0}
                                cStyle={styles.counterView} tStyle={styles.counterText} />
                        </TouchableOpacity>

                        <TouchableOpacity underlayColor='transparent' style={{ paddingRight: 15 }}
                            onPress={() => (this.state.restDetail) ? NavigationService.navigate("RestaurantDetails", { restDetail: this.state.restDetail }) : ""}>
                            <Image style={{ height: 22, width: 22 }} resizeMode='contain' source={Images.ic_about_us_white} />
                        </TouchableOpacity>
                    </Animated.View>

                </Animated.View>
                <StatusBar barStyle="light-content" />

                {/* ---------- Basket View ---------- */}
                {this.renderBasketView()}
            </View>
        );
    };
}

function mapStateToProps(state) {

    /*   const a = this.props.navigation;
      const navigateFrom = a.getParam('From', ''); */

    console.log("REST_MENU_STATEM:", JSON.stringify(state))



    return {
        basketData: state.basketData,
        internet: state.internet,

    }
}

// export default RestaurantMenu
export default connect(mapStateToProps, {})(RestaurantMenu)
