import React, { Component } from 'react'
import { Platform, StyleSheet, View, Dimensions, TouchableOpacity, Image, Text, SafeAreaView, StatusBar, PermissionsAndroid, DeviceEventEmitter } from 'react-native';
import styles from './styles'
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import MapView from 'react-native-maps'
import Geocoder from 'react-native-geocoding'
import { locationSelected } from '../../redux/actions'


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

var isFromAutoComplete = false

let locationPermission = false;

async function requestLocationPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            locationPermission = true;
            DeviceEventEmitter.emit(Constants.EVENTS.locationPermission, locationPermission)
        } else {
            locationPermission = false;
            alert(strings.gps_message)
        }
    } catch (err) {
        console.warn('error');
    }
}
class SearchAddress extends Component {
    constructor() {
        super()
        console.log("SEARCH_ADDRESS");
    }
    static navigationOptions = {
        header: null
    };

    state = {
        region: {
            // latitudeDelta: 0.0922,
            // longitudeDelta: 0.0421,
            latitudeDelta: Platform.OS === Constants.PLATFORM.android ? 0.5 * (screenWidth / screenHeight) : '',
            longitudeDelta: Platform.OS === Constants.PLATFORM.android ? 0.5 * (screenWidth / screenHeight) : '',
            latitude: 0,//25.1948475,
            longitude: 0,//55.2682899
        },
        address: '',
        addressForAutoSearch: strings.search_location,
        city: '',
        isShowMarker: false,
    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        /* const { navigation } = this.props;
        const navigateFrom = navigation.getParam('NavigateFrom', '');
        if (navigateFrom === '' || navigateFrom === 'SelectLocality') {
            return null
        } */

        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '20%'
                }
                }>
                    {this.renderBackButton()}


                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: "center",
                    width: '60%'
                }
                }>
                    <TextBold title={strings.search_location} textStyle={styles.textTitle} />
                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '20%'
                }
                }>

                </View>
                {/* <View style={{ flexDirection: 'row',  width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginRight: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View> */}
            </View>

        )
    }


    onRegionChange = region => {
        this.closeMarkerInfo()
    }

    onRegionChangeComplete = region => {

        if (!isFromAutoComplete) {
            this.setState({
                region: region
            })
        }

        this.getAddressFromLocation(region)

    }


    componentDidMount = () => {
        this.myLocation()
        // if(locationPermission){
        //     this.myLocation()
        // }
        // DeviceEventEmitter.addListener(Constants.EVENTS.locationPermission, (permission) => {
        //     this.myLocation();
        // })
        // requestLocationPermission();

        console.log("SEARCH_ADDRESS_STATE:componentDidMount:", JSON.stringify(this.props))
    }


    getAddressFromLocation = (region) => {
        Geocoder.getFromLatLng(region.latitude, region.longitude, 1)
            .then(json => {
                // console.log("ADDRESS:",JSON.stringify(json))

                var addressComponent = json.results[0].address_components;
                // console.log("\nADD:", JSON.stringify(addressComponent))

                this.getCityFromAddress(addressComponent);

                this.showMarkerInfo()

                if (!isFromAutoComplete) {
                    var formattedAddress = json.results[0].formatted_address;
                    this.setState({
                        address: formattedAddress,
                        addressForAutoSearch: formattedAddress
                    })
                } else {
                    isFromAutoComplete = !isFromAutoComplete
                }
                // console.warn(addressComponent)
                // console.log(addressComponent);
            })
            .catch(error =>
                console.warn(error),
                // this.setState({ address: '', addressForAutoSearch: strings.search_location,city:'' })
            );
    }


    getCityFromAddress = (addressComponent) => {
        for (let i = 0; i < addressComponent.length - 1; i++) {
            let locality = addressComponent[i]
            let types = locality.types
            for (let j = 0; j < types.length - 1; j++) {
                if (types[j] === 'locality') {
                    // console.log("LOCALITY:", locality.long_name)
                    this.setState({
                        city: locality.long_name
                    })
                }
            }
        }
    }

    myLocation = () => {

        // requestLocationPermission()
        // if (!locationPermission) {
        //     return
        // }
        this.watchID = navigator.geolocation.getCurrentPosition((position) => {
            // Create the object to update this.state.mapRegion through the onRegionChange function
            let region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: Platform.OS === Constants.PLATFORM.android ? 0.5 * (screenWidth / screenHeight) : '',
                longitudeDelta: Platform.OS === Constants.PLATFORM.android ? 0.5 * (screenWidth / screenHeight) : '',
                // latitudeDelta: 0.0922,
                // longitudeDelta: 0.0421,
                // latitudeDelta: 0.5,
                // longitudeDelta: 0.5 * (screenWidth / screenHeight)
            }


            // this.onRegionChange(region, region.latitude, region.longitude);

            this.setState({
                region
            });

            this.getAddressFromLocation(position.coords.latitude, position.coords.longitude)
            this.updatePinLocationOnMap(position.coords.latitude, position.coords.longitude)
            // alert(JSON.stringify(position.coords.latitude+" " +position.coords.longitude))
            // this.mapView.animateToRegion(region, 100);
        }, function (error) {
            // alert(error)
        });
    }

    //function to be pass to next screen and get result
    onSelect = data => {
        isFromAutoComplete = true;
        this.updatePinLocationOnMap(data.region.latitude, data.region.longitude)

        this.setState(prevState => ({
            region: {
                ...prevState.region,
                latitude: data.region.latitude,
                longitude: data.region.longitude
            },
            address: data.address,
            addressForAutoSearch: data.addressForAutoSearch
        }), () => {
            // isFromAutoComplete = false
        });

    };

    searchAddress = () => {
        this.props.navigate("AddressAutocomplete", { onSelect: this.onSelect });
    }



    updatePinLocationOnMap = (latitude, longitude) => {
        // console.log("LOCATION-" + latitude + ' - ' + longitude)
        let camera = {
            center: {
                latitude: latitude,
                longitude: longitude,
            },
            zoom: 16,
        }
        this.mapView.animateCamera(camera, 500)

    }

    onAccept = () => {
        this.props.locationSelected({ address: this.state.address, latitude: this.state.region.latitude, longitude: this.state.region.longitude, addressToDisplay: this.state.address, city: this.state.city })
        this.props.navigation.goBack();
    }

    closeMarkerInfo = () => {
        this.setState({
            isShowMarker: false
        })
    }

    showMarkerInfo = () => {
        this.setState({
            isShowMarker: true
        })
    }

    renderMarkerInfo = () => {
        if (this.state.isShowMarker) {
            return (
                <View style={{ alignItems: 'center' }}>
                    <View
                        style={styles.markerInfoView}>
                        <TouchableOpacity style={styles.markerClose} onPress={() => this.closeMarkerInfo()}>
                            <Image source={Images.ic_cross}
                                style={styles.markerCloseImage}></Image>
                        </TouchableOpacity>

                        <Text style={styles.markerText}>{this.state.address}</Text>
                    </View>

                    <Image source={Images.ic_location_arrow}
                        style={styles.markerBottomArrow}></Image>
                </View>

            )
        }
    }

    render() {
        const { region, address, addressForAutoSearch } = this.state
        return (

            <View style={{ width: '100%', flex: 1 }}>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                {this.renderToolbar()}

                <View style={styles.container}>

                    {/* --------------- Map View --------------- */}
                    <MapView
                        ref={(mapView) => { this.mapView = mapView; }}
                        style={styles.map}
                        provider={MapView.PROVIDER_GOOGLE}
                        initialRegion={region}
                        onRegionChangeComplete={this.onRegionChangeComplete}
                        onRegionChange={this.onRegionChange}
                    />

                    {/* --------------- AutoComplete Address View --------------- */}
                    <TouchableOpacity
                        style={styles.autoCompleteTouchable}
                        onPress={() => this.props.navigation.navigate('AddressAutocomplete', { onSelect: this.onSelect })}>
                        <TextBold title={addressForAutoSearch} numberOfLines={1} ellipsizeMode='tail'
                            textStyle={styles.autoCompleteText} />
                        <View style={{ width: '10%' }}>
                            <Image source={Images.ic_search} style={{ width: 30, height: 30 }}></Image>

                        </View>

                    </TouchableOpacity>


                    {/*--------------- maker view  ---------------*/}
                    <View
                        style={styles.markerOuterView}>
                        {this.renderMarkerInfo()}
                        <Image style={styles.marker} source={Images.ic_marker_pin} />

                    </View>

                    {/* --------------- MyLocation and Accept Button View --------------- */}
                    <SafeAreaView
                        style={styles.bottomSafeStyle}>
                        <View>
                            <TouchableOpacity style={styles.myLocation} onPress={() => this.myLocation()}>
                                <Image source={Images.ic_current_location} style={{ width: 28, height: 28 }}></Image>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.onAccept()}
                                style={{
                                    width: '100%',
                                    backgroundColor: Constants.color.primary,
                                    alignItems: 'center'
                                }}>
                                <TextRegular title={strings.accept}
                                    textStyle={styles.acceptText} />
                            </TouchableOpacity>
                        </View>
                    </SafeAreaView>
                </View></View>

        );
    };
}

function mapStateToProps(state) {
    console.log("SEARCH_ADDRESS_STATE:", JSON.stringify(state))
    return {
        address: state.address,
        addressForAutoSearch: state.address,
        region: {
            latitude: state.latitude,
            longitude: state.longitude,
            // latitudeDelta: 0.0922,
            // longitudeDelta: 0.0421,
        }
    }
}

export default connect(mapStateToProps, { locationSelected })(SearchAddress)