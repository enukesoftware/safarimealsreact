import React, { Component } from 'react'
import { Platform, StyleSheet, View, Image } from 'react-native';
import {Constants,strings,Images} from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'


export const Step1 = () => {

  return (
    <View style={styles.step1Container}>
      <TextBold title={strings.step1_text1.toUpperCase()} textStyle={[styles.headerTextStyle, styles.commonTextStyle]} />
      <Image style={styles.imageStyle} source={Images.step1_icon}></Image>
      <TextRegular title={strings.step1_text2} textStyle={[styles.step1TextStyle, styles.commonTextStyle]} />
      <TextBold title={strings.step1_text3} textStyle={[styles.commonTextStyle, styles.searchTextStyle]} />
      <TextRegular title={strings.step1_text4} textStyle={[styles.commonTextStyle, styles.findRestaurantTextStyle]} />
    </View>
  );
}

export const Step2 = () => {

  return (
    <View style={styles.step2Container}>
      <Image style={styles.imageStyle} source={Images.step2_icon}></Image>
      <TextRegular title={strings.step2_text2} textStyle={[styles.step1TextStyle, styles.commonTextStyle]} />
      <TextBold title={strings.step2_text3} textStyle={[styles.commonTextStyle, styles.searchTextStyle]} />
      <TextRegular title={strings.step2_text4} textStyle={[styles.commonTextStyle, styles.findRestaurantTextStyle]} />
    </View>
  );
}

export const Step3 = () => {

  return (
    <View style={styles.step2Container}>
      <Image style={styles.imageStyle} source={Images.step3_icon}></Image>
      <TextRegular title={strings.step3_text2} textStyle={[styles.step1TextStyle, styles.commonTextStyle]} />
      <TextBold title={strings.step3_text3} textStyle={[styles.commonTextStyle, styles.searchTextStyle]} />
      <TextRegular title={strings.step3_text4} textStyle={[styles.commonTextStyle, styles.findRestaurantTextStyle]} />
    </View>
  );
}

export const Step4 = () => {

  return (
    <View style={styles.step2Container}>
      <Image style={styles.imageStyle} source={Images.step4_icon}></Image>
      <TextRegular title={strings.step4_text2} textStyle={[styles.step1TextStyle, styles.commonTextStyle]} />
      <TextBold title={strings.step4_text3} textStyle={[styles.commonTextStyle, styles.searchTextStyle]} />
      <TextRegular title={strings.step4_text4} textStyle={[styles.commonTextStyle, styles.findRestaurantTextStyle]} />
    </View>
  );
}

// export default Step1
const styles = StyleSheet.create({
  step1Container: {
    flex: 1,
    alignItems: 'center',
    //    justifyContent:'center',
    padding: 32,
    backgroundColor: Constants.color.white
  },
  headerTextStyle: {
    fontSize: 18,
    marginTop: 25,
  },
  imageStyle: {
    height: 100,
    width: 100,
    marginTop: 20,
    resizeMode:'contain',
  },
  step1TextStyle: {
    marginTop: 5,
    fontSize: 14,
    opacity:0.9
  },
  commonTextStyle: {
    color: Constants.color.primary,
    textAlign: 'center',
  },
  searchTextStyle: {
    marginTop: 5,
    fontSize: 18,
  },
  findRestaurantTextStyle: {
    marginTop: 15,
    fontSize: 14,
    opacity:0.8,
  },

  step2Container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 32,
    backgroundColor: Constants.color.white
  }
});