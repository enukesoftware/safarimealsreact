import React, { Component } from 'react'
import { Platform, TextInput, SafeAreaView, Alert, Image, View, TouchableOpacity, Text, ActivityIndicator } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { ScrollView } from 'react-native-gesture-handler';
import { bookTable, bookingHistory, cancelBooking } from '../../services/APIService'
import { userDataAction, isLoginAction, } from '../../redux/actions'
import { connect } from 'react-redux'


let bookingHistoryList=[];
class TableBookingHistory extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            user_id: "",
            noDataFound: false,
            isLoading: false,

        }
    }

    componentDidMount() {
        if (this.props.userData) {
            const userData = this.props.userData
            userId = (userData.id) ? userData.id : ""
            this.setState({
            }, () => {
                this.setState({
                    user_id: userId
                })
                this.callBookingHistoryApi(userId)
            })
        }

    }

    callBookingHistoryApi = (userId) => {

        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }
        this.setState({
            isLoading: true,
        })
        let param = {
            user_id: userId,
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.bookingHistory


        console.log("URL:" + url + " PARAM:" + JSON.stringify(param))
        bookingHistory(url, param).then(res => {

            console.log("BOOKING_HISTORY_API_RES:" + JSON.stringify(res))


            if (res && res.success && res.data && res.data.data && res.data.data.length > 0) {
                bookingHistoryList= res.data.data,
                this.setState({
                    isLoading: false,
                })
            } else {
                this.setState({
                    isLoading: false,
                })
            }



            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                noDataFound: true,
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    // err = JSON.parse(err)
                    alert(err);
                    // this.showBookTableResponseAlert(false)
                }
            }, 100);
        });
    }

    callCancelBookingApi = (id,index) => {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }
        this.setState({
            isLoading: true,
        })

        let param = {
            user_id: this.state.user_id,
            order_id: id
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.cancelBooking


        console.log("URL:" + url + " PARAM:" + JSON.stringify(param))
        cancelBooking(url, param).then(res => {

            console.log("CANCEL_BOOKING_API_RES:" + JSON.stringify(res))


            if (res && res.success) {
                bookingHistoryList[index].status=6
                this.setState({
                    isLoading: false,
                }, () => {
                    
                })
            } else {
                this.setState({
                    // noDataFound: true,
                    isLoading: false,
                }, () => {
                    // this.showBookTableResponseAlert(false)
                })
            }



            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                // noDataFound: true,
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    // err = JSON.parse(err)
                    alert(err);
                    // this.showBookTableResponseAlert(false)
                }
            }, 100);
        });
    }

    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={styles.activityIndicatorView}>
                    <View style={styles.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={this.state.loading} />
                    </View>
                </View>

            )
        } else {
            return (
                <View />
            )
        }

    }

    calculateStatus(status) {
        switch (status) {
            case Constants.TABLEBOOKING_STATUS.pending:
                return strings.cancel
            case Constants.TABLEBOOKING_STATUS.completed:
                return strings.completed
            case Constants.TABLEBOOKING_STATUS.cancelled:
                return strings.cancelled
            default:
                return strings.cancel
        }
    }

    renderBookingHistoryItem = (item,index) => {
    
        let orderNumber = ''
        let name = ''
        let address = ''
        let bookDate = ''
        let status = ''
        let bookTime = ''
        let guests = ''
        let stringStatus = ''
        let restImage = Images.no_restaurant
        if (item) {
            status = (item.status) ? parseInt(item.status) : ''
            stringStatus = this.calculateStatus(status)
            orderNumber = (item.order_number) ? item.order_number : ''
            name = (item.restaurant && item.restaurant.name) ? item.restaurant.name : ''
            if (item.restaurant) {
                let floor = (item.restaurant.floor && item.floor != '') ? item.restaurant.floor + ' ' + strings.floor_text + ', ' : ''
                let street = (item.restaurant.street && item.restaurant.street != '') ? item.restaurant.street + ', ' : ''
                let city = ''
                if (item.restaurant.city && item.restaurant.city.name && item.restaurant.city.name != '') {
                    city = item.restaurant.city.name
                }
                address = floor + street + city

                restImage = (item.restaurant.image && item.restaurant.image.location && item.restaurant.image.location != '') ? { uri: item.restaurant.image.location } : Images.no_restaurant

            }

            bookDate = (item.book_date) ? item.book_date : ''
            bookTime = (item.book_time) ? item.book_time : ''
            guests = (item.total_person) ? item.total_person : ''
        }
        return (
            <View>
                {/* View For Image and Items*/}
                <View style={{ flexDirection: 'row' }}>
                    {/* View For Image */}
                    <View style={styles.viewImg}>
                        <Image source={restImage} resizeMode='contain'
                            style={styles.imgLogo} ></Image>
                    </View>
                    {/* View For Items */}
                    <View style={styles.viewItems}>
                        <TextBold title={name} textStyle={styles.textItemName} />
                        <TextRegular title={address} textStyle={styles.textItemsAdd} />
                        <View style={{ width: '50%', flexDirection: "row", alignItems: "center", padding: 3, }}>
                            <TextBold title={strings.booking_id} textStyle={{ fontSize: 12 }} />
                            <TextRegular title={orderNumber} textStyle={styles.textItemsAdd} />
                        </View>


                        <View style={styles.viewInputRow}>
                            <TextBold title={strings.booking_date} textStyle={{ fontSize: 12 }} />
                            <TextRegular title={bookDate} textStyle={styles.textItems} />
                        </View>
                        <View style={styles.viewInputRow}>
                            <View style={{ flexDirection: "row", width: '50%' }}>
                                <TextBold title={strings.timing} textStyle={{ fontSize: 12 }} />
                                <TextRegular title={bookTime} textStyle={styles.textItems} />
                            </View>
                            <View style={{ flexDirection: "row", width: '50%', justifyContent: "flex-end" }}>

                                <TextBold title={strings.guests} textStyle={{ fontSize: 12, marginLeft: 10 }} />
                                <TextRegular title={guests} textStyle={styles.textItems} />
                            </View>

                        </View>



                    </View>
                </View>
                {/* View For Buttons */}
                <View style={{ flexDirection: 'row', backgroundColor: 'white', alignItems: 'center' }}>
                    {/* <View style={{ width: '50%', flexDirection: "row", alignItems: "center" }}>
                        <TextRegular title= textStyle={{ fontWeight: "bold", fontSize: 12 }}>{"Order No :"}</Text>
                        <TextRegular title= textStyle={styles.textItemsAdd} >{orderNumber}</Text>
                    </View> */}
                    <View style={[styles.viewInputRow, { width: '100%', justifyContent: 'flex-end' }]}>


                        {/* Button Action */}
                        {status != 1 ?
                            <View style={styles.buttonAction2}>
                                <TextBold title={stringStatus} textStyle={styles.textActionButton2} />
                            </View>
                            :
                            <TouchableOpacity style={styles.buttonAction}
                                onPress={() => this.cancleClicked(orderNumber,index)}>
                                <TextBold title={stringStatus} textStyle={styles.textActionButton} />
                            </TouchableOpacity>
                        }
                    </View>

                </View>
            </View>
        )
    }

    cancleClicked = (orderNumber,index) => {
        Alert.alert(
            strings.cancel + " " + strings.booking,
            strings.cancel_booking_message,
            [
                //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {
                    text: strings.no,
                    style: 'cancel',
                },
                {
                    text: strings.yes,
                    onPress: () => this.callCancelBookingApi(orderNumber,index)
                },
            ],
            { cancelable: false },
        );
    }

    _orderlist = () => {
        return bookingHistoryList.map((item, index) =>

            // Main Card 
            <TouchableOpacity key={index} style={styles.mainCard}>
                {this.renderBookingHistoryItem(item,index)}
            </TouchableOpacity>)
    }
    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.side_menu_booking_history} textStyle={styles.textTitle} />

                </View>
            </View>

        )
    }

    render() {
        console.log("bookingHistoryList:" + JSON.stringify(bookingHistoryList))   
        return (
            <View style={{}} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                
                <View style={{ height: '100%', width: '100%' }}>
                    {this.renderToolbar()}
                    <View style={styles.loginForm}>
                        <ScrollView contentInsetAdjustmentBehavior="automatic"
                            keyboardShouldPersistTaps='always'
                            style={{}}>
                            {this._orderlist()}
                        </ScrollView>
                    </View>
                </View>
                {this.renderProgressBar()}
            </View>
        );
    }
}


function mapStateToProps(state) {
    
    return {
        userData: state.userData,
        internet: state.internet,
        isLogin: state.isLogin,
    }
}

export default connect(mapStateToProps, { isLoginAction, userDataAction, })(TableBookingHistory)







