import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, Animated, TextInput, TouchableOpacity, Image, View, ImageBackground, Text, ActivityIndicator, AsyncStorage } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle,callUpdateDeviceTokenApi } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { ScrollView } from 'react-native-gesture-handler';
import { TextInputLayout } from 'rn-textinputlayout';
import { checkNumber, signInApi } from '../../services/APIService'
import NavigationService from '../../services/NavigationServices'
import { isLoginAction, userDataAction } from '../../redux/actions'

let previousPage = "Dashboard"
class Login extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            mobile: "",
            showall: true,
            password: "",
        }
    }


    componentDidMount() {
        const { navigation } = this.props;
        previousPage = navigation.getParam('previousPage', "Dashboard");
    }


    //validation function on click continue
    _validateAndSubmit = () => {
        if (!this.state.showall) {
            if (this.state.mobile == "" || this.state.mobile.length < 10) {
                alert(strings.correct_mobile_number_error)
                return
            }
            this.callCheckNumberApi()
        } else {
            if (this.state.mobile == "" || this.state.mobile.length < 10) {
                alert(strings.correct_mobile_number_error)
                return
            }

            if (this.state.password == "") {
                alert(strings.please_enter_password)
                return
            }

            this.callLoginApi()

        }


        // if (this.state.mobile.length < 10 || isNaN(this.state.mobile)) {
        //     alert("Please Enter Valid Mobile Number")
        //     return
        // }
        // switch (this.state.showall) {
        //     case true:
        //         if (this.state.password == "") {
        //             alert("Please Enter a Password")
        //             return
        //         }
        //         //// here you do login
        //         break;
        //     case false:
        //         this.setState({
        //             showall: true
        //         })
        //         break;
        // }
    }

    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>

            )
        } else {
            return
        }

    }

    callCheckNumberApi() {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }

        const param = {
            contact_number: this.state.mobile
        }

        this.setState({
            isLoading: true,
        })

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.checkNumber

        checkNumber(url, param).then(res => {

            console.log("CHECK_NUMBER_API_RES:" + JSON.stringify(res))
            // this.setState({
            //     isLoading: false,
            // })

            if (res && res.success) {
                this.setState({
                    isLoading: false,
                })
                if (res.number_exist) {
                    this.setState({
                        showall: true,
                    })

                } else {
                    // this.setState({
                    //     showall: false,
                    // })
                    NavigationService.navigate("Register", { mobile: this.state.mobile, previousPage: previousPage })
                }

            } else {
                this.setState({
                    isLoading: false,
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });
    }

    callLoginApi() {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }

        const param = {
            mobile: this.state.mobile,
            password: this.state.password,
        }

        this.setState({
            isLoading: true,
        })

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.signIn

        signInApi(url, param).then(res => {

            console.log("SIGN_IN_API_RES:" + JSON.stringify(res))
            // this.setState({
            //     isLoading: false,
            // })

            if (res && res.success) {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res.data) {
                        AsyncStorage.setItem(Constants.STORAGE_KEY.userData, JSON.stringify(res.data));
                        AsyncStorage.setItem(Constants.STORAGE_KEY.isLogin, JSON.stringify({ isLogin: true }));
                        this.props.userDataAction(res.data)
                        this.props.isLoginAction(true)
                        callUpdateDeviceTokenApi()
                        NavigationService.navigate(previousPage)
                    }

                })
            } else {
                this.setState({
                    isLoading: false,
                })
                if (res && res.message) {
                    alert(res.message)
                }else if(res && res.error){
                    alert(res.error)
                }
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });
    }

   


    onBackClick = () => {
        NavigationService.goBack()
        // if (this.state.showall) {
        //     this.setState({
        //         isLoading: false,
        //         mobile: "",
        //         showall: false,
        //         password: "",
        //     })
        // } else {
        //     NavigationService.goBack()
        // }
    }

    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }

    renderToolbar() {
        return (
            <View style={styles.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '100%'
                }
                }>
                    {this.renderBackButton()}

                </View>
            </View>

        )
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={Images.pizza} style={styles.backgroundImage}>
                    {this.renderToolbar()}
                    <View style={{ flex: 3, justifyContent: 'flex-end' }}>
                        <TextBold title={strings.login} textStyle={{ fontSize: 25, color: Constants.color.white, padding: 25 }} />

                    </View>
                    <View style={styles.loginForm}>
                        <ScrollView contentInsetAdjustmentBehavior="automatic"
                            keyboardShouldPersistTaps='always'
                            style={{}}>
                            {/* Phone */}
                            <View style={styles.viewInput}>
                                <Image source={Images.phone} style={styles.iconLeft}></Image>
                                <TextInputLayout
                                    style={styles.inputLayout}
                                    hintColor={Constants.color.white}
                                    focusColor={Constants.color.white}>
                                    <TextInput
                                        maxLength={10}
                                        ref='mobileNo'
                                        keyboardType="numeric"
                                        value={this.state.mobile}
                                        onChangeText={(text) => {
                                            this.setState({
                                                mobile: text.trim()
                                            });
                                        }}
                                        style={styles.textInput}
                                        placeholder={strings.hint_phone_no} />
                                </TextInputLayout>
                            </View>
                            {/*Hidden Content*/}
                            {this.state.showall ?
                                <View>
                                    {/* Password*/}
                                    <View style={styles.viewInput}>
                                        <Image source={Images.ic_password} style={styles.iconLeft}></Image>
                                        <TextInputLayout
                                            style={styles.inputLayout}
                                            hintColor={Constants.color.white}
                                            focusColor={Constants.color.white}>
                                            <TextInput
                                                onChangeText={(text) => {
                                                    this.setState({
                                                        password: text
                                                    });
                                                }}
                                                secureTextEntry={true}
                                                style={styles.textInput}
                                                placeholder={strings.hint_password} />
                                        </TextInputLayout>
                                    </View>
                                    {/*Forget Password */}
                                    <TouchableOpacity
                                        style={styles.forgetTouch}
                                        onPress={() => {
                                            this.props.navigation.navigate('ForgetPassword');
                                        }} >
                                        <TextRegular
                                            title={strings.forget_password}
                                            textStyle={styles.textForgetPass} />
                                    </TouchableOpacity>
                                </View>


                                // </View>
                                :
                                null
                            }

                            {/*Button Login */}
                            <View style={styles.viewButton}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this._validateAndSubmit();
                                    }}
                                    style={styles.touchOpacity}>
                                    <TextRegular textStyle={styles.textButtonAdd} title={this.state.showall ? strings.login_button : strings.continue}></TextRegular>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'center' }}>

                                <TextRegular title={strings.dont_have_account} textStyle={{ fontSize: 16, color: Constants.color.white, }} />
                                <TouchableOpacity
                                    onPress={() => {
                                        NavigationService.navigate("Register", { mobile: this.state.mobile, previousPage: previousPage })
                                    }}>
                                    <TextBold title={strings.register_button} textStyle={{ fontSize: 16, color: Constants.color.primary}} />
                                </TouchableOpacity>

                            </View>

                        </ScrollView>
                    </View>
                </ImageBackground>
                {this.renderProgressBar()}
            </View>

        );

    }

}

function mapStateToProps(state) {
    return {
        internet: state.internet
    }
}

export default connect(mapStateToProps, { isLoginAction, userDataAction })(Login)






