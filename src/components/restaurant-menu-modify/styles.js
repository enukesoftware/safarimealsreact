import { StyleSheet } from 'react-native';
import { Constants } from '../../utils'

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        // paddingTop: Constants.AppConstant.statusBarHeight,
    },
    restaurantImage: {
        height: 60,
        width: 60,
        alignSelf: 'center',
        marginTop: 10,
        borderColor: 'white',
        borderWidth: 2,
        borderRadius: 60 / 2,
    },
    restName: {
        color: Constants.color.fontWhite,
        textAlign: 'center',
        marginTop: 10,
    },
    headerShadow: {
        position: 'absolute',
        opacity: 0.7,
        backgroundColor: Constants.color.black,
        width: '100%',
        height: '100%'
    },
    bottomView: {
        backgroundColor: Constants.color.gray,
        flexDirection: 'row',
        padding: 10,
        // alignSelf:'flex-end'
    },
    bottomViewBlock: {
        // flex: 0.4,
        flexDirection: 'row',
        // paddingHorizontal: 5,
        // paddingVertical: 10,
        alignItems: 'center'
    },
    blockImage: {
        width: 12,
        height: 12
    },
    blockDivider: {
        height: '100%',
        width: 1,
        backgroundColor: Constants.color.dark_gray
    },
    blockLabel: {
        fontSize: Constants.fontSize.SmallXX,
        opacity: 0.5,
        paddingTop: 5
    },
    blockValue: {
        // fontSize: Constants.fontSize.SmallX,
        paddingLeft: 5,
        color: Constants.color.fontBlack,
        flexWrap: 'wrap',
        // backgroundColor:'red'
    },
    basketViewBlock: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor:Constants.color.primary,
    },
    basketView:{
        
        flexDirection:'row',
        paddingVertical:6,
        paddingHorizontal:15,
    },
    basketViewLeft:{
        flexDirection:'row',
        width:'60%'
    },
    basketViewRight:{
        width:'40%',
        alignItems:'flex-end',
        justifyContent:'center'
    },
    viewBasketTouchable:{
        borderWidth:1,
        borderColor:Constants.color.white_alpha_fifty,
        borderRadius:5,
        // opacity:0.5
        // alignItems:'flex-end',
    },
    viewBasketText:{
        color:Constants.color.fontWhite,
        fontSize:Constants.fontSize.SmallX,
        paddingVertical:2,
        paddingHorizontal:10
    },
    activityIndicatorView:{
        height: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
    },
    activityIndicatorWrapper:{
        backgroundColor: Constants.color.gray,
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.8,
    },
    counterView:{
        position:'absolute',
        top:0,
        right:8,
        height:18,
        width:18,
        borderRadius: 9
    },
    counterText:{
        fontSize: Constants.fontSize.SmallXXX,
    }
})