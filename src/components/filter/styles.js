import { Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({
    contentContainer: {
    },
    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 25,
    },
    safeStyle: {
        flex: 1,
        backgroundColor: 'transparent',
        alignItems: 'center',
        height: '100%',
        width: '100%',
    },
    container: {
        flex: 1,
        paddingTop: Constants.AppConstant.statusBarHeight,
    },
    infoContainer: {
        flex: 1,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textContainer: {
        flex: 1,
    },
    map: {
        // ...StyleSheet.absoluteFillObject,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    bottomSafeStyle: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%'
    },
    imageInfo: {
        height: 90,
        width: 90,
        margin: 15,
    },
    textInfo: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold',
    },
    weekRow: {
        flexDirection: "row",
        marginHorizontal: 20,
    },
    viewHead: {
        backgroundColor: '#808080',
        width: '100%',
        height: 1,
        marginTop: 25
    },

    textHead: {
        color: 'black', marginHorizontal: 20, marginTop: 25, marginBottom: 10,
    },
    textData: {
        color: '#808080',
        marginHorizontal: 22,
        marginVertical: 5,
        fontWeight: 'normal',
    },
    TextResetNavigation: {
        fontSize: Constants.fontSize.NormalXX,
        color: Constants.color.fontWhite,
    },
    rowCheckbox: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 22
    },
    rightTextStyle: {
        fontFamily: Fonts.Regular,
        fontSize: 16,
    },






})