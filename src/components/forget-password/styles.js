import {Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({

    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle:{
        fontSize: 20,
        color:Constants.color.white,
        marginLeft:15,
    },
    loginForm: {
        padding: 15,
        width: '100%',
        flex: 9.3
    },
    textInput: {
        fontSize: 18,
        fontFamily: Fonts.Regular,
        height: 40,
        width: '100%'
    },
    inputLayout: {
        width: '80%',
        marginHorizontal: 10
    },
    iconLeft: {
        height: 30,
        width: 30,
        margin: 5
    },
    viewInput: {
        paddingTop: 10,
        flexDirection: "row",
        alignItems: 'flex-end',
    },
    imgBack: {
        width: '100%',
        height: '100%'
    },
    touchOpacity: {
        backgroundColor: Constants.color.primary,
        justifyContent: "center",
        alignItems: "center",
        width: '55%',
        padding: 18,
        borderRadius: 5
    },
    textButtonAdd: {
        color: Constants.color.white,
        fontSize: 16,
    },
    viewButton: {
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: 30,
    },


})
