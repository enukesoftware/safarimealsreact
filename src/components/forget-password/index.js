import React, { Component } from 'react'
import {Platform, TextInput, SafeAreaView, Image, View, TouchableOpacity, Text,ActivityIndicator } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { TextInputLayout } from 'rn-textinputlayout';
import { ScrollView } from 'react-native-gesture-handler';
import { forgotPasswordApi } from '../../services/APIService'
import { connect } from 'react-redux'
import NavigationServices from '../../services/NavigationServices';


class ForgetPassword extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading:false,
            email: "",
        }
    }
    // validation function
        _verifyAndRequest = () => {
            let emailcontext = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            
            if (this.state.email == "") {
                alert(strings.email_error)
                return
            }
            if (!emailcontext.test(this.state.username)) {
                alert(strings.email_id_error)
                return
            }

            //here u Request password
            this.callForgotPasswordApi()
        }

        showAlert = (message) => {
            Alert.alert(
                strings.forget_password,
                message,
                [
                    //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    // {
                    //     text: strings.cancel,
                    //     onPress: () => console.log('Cancel Pressed'),
                    //     style: 'cancel',
                    // },
                    {
                        text: strings.ok,
                        onPress: () => NavigationServices.goBack()
                    },
                ],
                { cancelable: false },
            );
        }

        callForgotPasswordApi() {
            if(!this.props.internet){
                alert(strings.message_lno_internet)
                return
            }
            const param = {
                email: this.state.email
            }
    
            this.setState({
                isLoading: true,
            })
    
            let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.forgotPassword
    
            forgotPasswordApi(url, param).then(res => {
    
                console.log("FORGOT_PASSWORD_API_RES:" + JSON.stringify(res))
                // this.setState({
                //     isLoading: false,
                // })
    
                if (res && res.success) {
                    this.setState({
                        isLoading: false,
                    },()=>{
                        this.showAlert(res.message)
                    })
                    
    
                } else {
                    this.setState({
                        isLoading: false,
                    },()=>{
                        alert(res.error)
                    })
                }
    
            }).catch(err => {
                this.setState({
                    isLoading: false,
                })
                setTimeout(() => {
                    if (err) {
                        alert(JSON.stringify(err));
                    }
                }, 100);
            });
        }

        onBackClick = () => {
            const { navigation } = this.props;
            navigation.goBack();
        }
    
        renderBackButton() {
            if (Platform.OS === Constants.PLATFORM.ios) {
                return (
                    <TouchableOpacity activeOpacity={0.8}
                        onPress={() => this.onBackClick()}>
                        <Image source={Images.ic_back_ios}
                            style={{ marginLeft: 5, height: 22, width: 22, }}
                        ></Image>
                    </TouchableOpacity>
                )
            } else {
                return (
                    <TouchableOpacity activeOpacity={0.8}
                        onPress={() => this.onBackClick()}>
                        <Image source={Images.ic_back_android}
                            style={{ marginLeft: 15, height: 35, width: 25, }}
                        ></Image>
                    </TouchableOpacity>
                )
            }
        }
    
    
        renderToolbar() {
            return (
                <View style={GlobalStyle.toolbar}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        width: '90%'
                    }}>
                        {this.renderBackButton()}
                        <TextBold title={strings.forget_pasword_title} textStyle={styles.textTitle}/>
    
                    </View>
                </View>
    
            )
        }


    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>

            )
        } else {
            return
        }

    }

   
    render() {

        return (
            <View style={{}} >
            <SafeAreaView style={{backgroundColor:Constants.color.primary}}></SafeAreaView>
           <View style={{height:'100%',width:'100%'}}>
               {this.renderToolbar()}
             
                <View style={styles.loginForm}>
                        <ScrollView contentInsetAdjustmentBehavior="automatic"
                         keyboardShouldPersistTaps='always'
                         style={{}}>

                          
                            {/* Email Id*/}
                            <View style={styles.viewInput}>
                                <Image source={Images.ic_email} style={styles.iconLeft}></Image>
                                <TextInputLayout
                                    style={styles.inputLayout}
                                    focusColor={Constants.color.black}>
                                    <TextInput
                                        ref='email'
                                        onChangeText={(text) => {
                                            this.setState({
                                                email: text.trim()
                                            });
                                        }}
                                        style={styles.textInput}

                                        placeholder={strings.hint_email_id} />
                                </TextInputLayout>
                            </View>

                            {/*Button Request */}
                            <View style={styles.viewButton}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this._verifyAndRequest();
                                    }}
                                    style={styles.touchOpacity}>
                                    <TextBold title={strings.request_password} textStyle={styles.textButtonAdd}/>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </View>   
                {this.renderProgressBar()}
            </View>
        );
    }
}

function mapStateToProps(state) {
    //console.log("BASKET_STATE:" + JSON.stringify(state.basketData))
    return {
        internet:state.internet,
    }
}

export default connect(mapStateToProps, {})(ForgetPassword)






