import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, TextInput, SafeAreaView, TouchableHighlight, Image, View, TouchableOpacity, Text, ActivityIndicator, AsyncStorage } from 'react-native';
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle, callUpdateDeviceTokenApi } from '../../utils'
import { TextInputLayout } from 'rn-textinputlayout';
import { Dialog } from 'react-native-simple-dialogs';
import { ScrollView } from 'react-native-gesture-handler';
import { signUpApi, sendOtpApi } from '../../services/APIService'
import NavigationService from '../../services/NavigationServices';
import { isLoginAction, userDataAction } from '../../redux/actions'
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";


let previousPage = "Dashboard"
class Register extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            name: "",
            firstName: "",
            lastName: "",
            email: "",
            mobile: "",
            password: "",
            confirmpass: "",
            isOTPVisible: false,
            otp: "",
            countrycode: Constants.DEFAULT_COUNTRY_CODE,
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        const mobile = navigation.getParam('mobile', "");
        previousPage = navigation.getParam('previousPage', "Dashboard");

        this.setState({
            mobile: mobile,
        })

        // AsyncStorage.getItem(Constants.STORAGE_KEY.adminData, (error, result) => {
        //     if (error) {
        //         console.log("ERROR:" + JSON.stringify(error))
        //     }
        //     else {
        //         if (result) {
        //             console.log("ADMIN_SAVED_DATA:" + result)
        //             result = JSON.parse(result)
        //             if (result.countrycode) {
        //                 this.setState({
        //                     countrycode: result.countrycode,
        //                 })
        //             }

        //         }
        //     }
        // })
    }

    onCountrySelect = data => {
        console.log("COUNTRY_DATA:" + JSON.stringify(data))
        this.setState({
            countrycode: data.selectedCountry.countryCode,
        })
    };


    //function for verify and Register
    _verifyAndRegister = () => {
        let emailcontext = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;


        if (this.state.firstName == "") {
            alert(strings.firstName_error)
            return
        }
        if (this.state.firstName.length < 4) {
            alert(strings.correct_name_error)
            return
        }

        if (this.state.lastName == "") {
            alert(strings.lastNamed_error)
            return
        }

        if (this.state.email == "") {
            alert(strings.email_error)
            return
        }
        if (!emailcontext.test(this.state.email)) {
            alert(strings.email_id_error)
            return
        }
        if (this.state.mobile == "") {
            alert(strings.phone)
            return
        }
        if (this.state.mobile.length < 10 || isNaN(this.state.mobile)) {
            alert(strings.phone_no_error)
            return
        }
        if (this.state.password.length < 6) {
            alert(strings.password_error)
            return
        }
        if (this.state.confirmpass == "") {
            alert(strings.confirm_pass_error)
            return
        }
        if (this.state.password != this.state.confirmpass) {
            alert(strings.password_match_error)
            return
        }
        //here u do Registration
        // this.setState({ isOTPVisible: true })

        this.setState({
            isOTPVisible: true
        }, () => {
            this.callSendOtpApi()
        })

    }

    callSendOtpApi() {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }

        const param = {
            full_name: this.state.firstName + " " + this.state.lastName,
            contact_number: this.state.mobile,
            email: this.state.email,
            password: this.state.password,
            countrycode: this.state.countrycode,
        }

        this.setState({
            isLoading: true,
        })

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.sendOtp

        sendOtpApi(url, param).then(res => {

            console.log("SEND_OTP_API_RES:" + JSON.stringify(res))
            // this.setState({
            //     isLoading: false,
            // })

            if (res && res.success) {
                this.setState({
                    isLoading: false,
                })


            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    } else if (res && res.message) {
                        alert(res.message)
                    }
                })


            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });
    }

    callRegisterApi() {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }

        this.setState({
            isLoading: true,
            // isOTPVisible: false
        })

        const param = {
            full_name: this.state.firstName + " " + this.state.lastName,
            contact_number: this.state.mobile,
            email: this.state.email,
            password: this.state.password,
            countrycode: this.state.countrycode,
            otp: this.state.otp
        }



        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.signUp

        signUpApi(url, param).then(res => {

            console.log("REGISTER_API_RES:" + JSON.stringify(res))
            // this.setState({
            //     isLoading: false,
            // })

            if (res && res.success) {
                this.setState({
                    isLoading: false,
                    isOTPVisible: false
                }, () => {
                    if (res.data) {
                        AsyncStorage.setItem(Constants.STORAGE_KEY.userData, JSON.stringify(res.data));
                        AsyncStorage.setItem(Constants.STORAGE_KEY.isLogin, JSON.stringify({ isLogin: true }));
                        this.props.userDataAction(res.data)
                        this.props.isLoginAction(true)
                        callUpdateDeviceTokenApi()
                        NavigationService.navigate(previousPage)
                    }

                })
            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    } else if (res && res.message) {
                        alert(res.message)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });
    }

    _verifyOTP = () => {
        if (this.state.otp == "") {
            alert("Please Enter OTP")
            return
        }
        if (this.state.otp.length < 6) {
            alert("Please Enter Valid OTP")
            return
        }

        // verify OTP here
        // this.setState({ isOTPVisible: false })

        this.callRegisterApi()
    }
    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }


    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>

            )
        } else {
            return
        }

    }


    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.register_button} textStyle={styles.textTitle} />

                </View>
            </View>

        )
    }


    renderOtpView() {
        if (!this.state.isOTPVisible) {
            return
        }
        return (
            <View style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                alignItems: 'center',
                justifyContent: 'center',
            }}
            >
                <View style={{
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    backgroundColor: Constants.color.black,
                    opacity: 0.7,
                }}
                />
                <View style={{ backgroundColor: Constants.color.gray, padding: 10, margin: 30, borderRadius: 5 }}>
                    <View style={styles.viewInput}>

                        <TextInputLayout
                            style={[styles.inputLayout, { width: '95%' }]}
                            focusColor={Constants.color.black}>
                            <TextInput
                                autoFocus={false}
                                style={styles.textInput}
                                textAlign={'center'}
                                autoCapitalize='characters'
                                maxLength={6}
                                onChangeText={(text) => {
                                    this.setState({
                                        otp: text.trim()
                                    });
                                }}
                                placeholder={strings.enter_otp} />

                        </TextInputLayout>
                    </View>
                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'flex-end', paddingTop: 20 }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.callSendOtpApi()
                            }}
                            style={styles.okOTPButton}>
                            <TextRegular title={strings.resend_otp} textStyle={styles.textButtonAdd} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    isOTPVisible: false
                                })
                            }}
                            style={styles.okOTPButton}>
                            <TextRegular title={strings.cancel} textStyle={styles.textButtonAdd} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                this._verifyOTP();
                            }}
                            style={styles.okOTPButton}>
                            <TextRegular title={strings.ok} textStyle={styles.textButtonAdd} />
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>

                <View style={{ flex: 1, backgroundColor: Constants.color.white }} >
                    {this.renderToolbar()}



                    <View style={styles.loginForm}>
                        {/* <ScrollView contentInsetAdjustmentBehavior="automatic"
                            keyboardShouldPersistTaps='always'
                            style={{}}> */}
                        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ width: '100%', flex: 1 }} >
                            <View style={{ padding: 20, flex: 1, marginBottom: 100 }}>
                                {/*Full Name*/}

                                <View style={{ flexDirection: 'row' }}>
                                    <View style={[styles.viewInput, { width: '44%', paddingEnd: 10 }]}>
                                        <Image source={Images.ic_person} style={styles.iconLeft}></Image>
                                        <TextInputLayout
                                            style={styles.inputLayout}
                                            focusColor={Constants.color.black}>
                                            <TextInput
                                                ref='firstName'
                                                style={styles.textInput}
                                                onChangeText={(text) => {
                                                    this.setState({
                                                        firstName: text.trim()
                                                    });
                                                }}
                                                placeholder={strings._hint_first_name} />

                                        </TextInputLayout>
                                    </View>
                                    <View style={[styles.viewInput, { width: '44%', paddingStart: 10 }]}>
                                        <Image source={Images.ic_person} style={styles.iconLeft}></Image>
                                        <TextInputLayout
                                            style={styles.inputLayout}
                                            focusColor={Constants.color.black}>
                                            <TextInput
                                                ref='lastName'
                                                style={styles.textInput}
                                                onChangeText={(text) => {
                                                    this.setState({
                                                        lastName: text.trim()
                                                    });
                                                }}
                                                placeholder={strings.hint_last_name} />

                                        </TextInputLayout>
                                    </View>
                                </View>

                                {/* Email Id*/}
                                <View style={styles.viewInput}>
                                    <Image source={Images.ic_email} style={styles.iconLeft}></Image>
                                    <TextInputLayout
                                        style={styles.inputLayout}
                                        focusColor={Constants.color.black}>
                                        <TextInput
                                            ref='email'
                                            onChangeText={(text) => {
                                                this.setState({
                                                    email: text.trim()
                                                });
                                            }}
                                            style={styles.textInput}

                                            placeholder={strings.hint_email_id} />
                                    </TextInputLayout>
                                </View>

                                {/* Phone */}
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={[styles.viewInput, { width: '32%', paddingEnd: 10 }]}>
                                        <Image source={Images.phone} style={styles.iconLeft}></Image>
                                        <TouchableOpacity onPress={() => NavigationService.navigate("CountrySelection", {
                                            onCountrySelect: this.onCountrySelect,
                                            selectedCountry: this.state.countrycode
                                        })}>
                                            <TextInputLayout
                                                editable={false}
                                                style={styles.inputLayout}
                                                focusColor={Constants.color.black}>
                                                <TextInput
                                                    editable={false}
                                                    maxLength={6}
                                                    ref='countryCode'
                                                    keyboardType="numeric"
                                                    value={this.state.countrycode}
                                                    style={styles.textInput}
                                                    placeholder={strings.countrycode_hint} />
                                            </TextInputLayout>
                                        </TouchableOpacity>

                                    </View>
                                    <View style={[styles.viewInput, { width: '60%', paddingStart: 10 }]}>
                                        <Image source={Images.phone} style={styles.iconLeft}></Image>
                                        <TextInputLayout
                                            style={styles.inputLayout}
                                            focusColor={Constants.color.black}>
                                            <TextInput
                                                maxLength={10}
                                                ref='mobileNo'
                                                keyboardType="numeric"
                                                onChangeText={(text) => {
                                                    this.setState({
                                                        mobile: text.trim()
                                                    });
                                                }}
                                                value={this.state.mobile}
                                                style={styles.textInput}
                                                placeholder={strings.hint_phone_no} />
                                        </TextInputLayout>
                                    </View>

                                </View>

                                {/* Password*/}
                                <View style={styles.viewInput}>
                                    <Image source={Images.ic_password} style={styles.iconLeft}></Image>
                                    <TextInputLayout
                                        style={styles.inputLayout}
                                        focusColor={Constants.color.black}>
                                        <TextInput
                                            ref='password'
                                            onChangeText={(text) => {
                                                this.setState({
                                                    password: text
                                                });
                                            }}
                                            secureTextEntry={true}
                                            style={styles.textInput}
                                            placeholder={strings.hint_password} />
                                    </TextInputLayout>
                                </View>
                                {/* Confirm  Password*/}
                                <View style={styles.viewInput}>
                                    <Image source={Images.ic_password} style={styles.iconLeft}></Image>
                                    <TextInputLayout
                                        style={styles.inputLayout}
                                        focusColor={Constants.color.black}>
                                        <TextInput
                                            ref='confirmpass'
                                            onChangeText={(text) => {
                                                this.setState({
                                                    confirmpass: text
                                                });
                                            }}
                                            secureTextEntry={true}
                                            style={styles.textInput}
                                            placeholder={strings.hint_confirmpassword} />
                                    </TextInputLayout>
                                </View>
                                {/* OTP Dialog */}
                                {/* <Dialog
                                visible={this.state.isOTPVisible}
                                title={strings.otp_sent}
                                titleStyle={styles.titleStyle}
                                animationType='slide'
                            >
                                
                            </Dialog> */}
                                {/*Button Register */}
                                <View style={styles.viewButton}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this._verifyAndRegister();
                                        }}
                                        style={styles.touchOpacity}>
                                        <TextBold title={strings.register_button} textStyle={styles.textButtonAdd} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </KeyboardAwareScrollView>
                    </View>
                    {this.renderOtpView()}
                    {this.renderProgressBar()}
                </View>

            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        // location: state.location,
        // address: state.location.addressToDisplay,
        // city: state.location.city,
        // isCloseVisible: state.location.addressToDisplay === strings.hint_location ? false : true,,
        internet: state.internet
    }
}

export default connect(mapStateToProps, { isLoginAction, userDataAction })(Register)







