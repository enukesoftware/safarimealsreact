import React, { Component } from 'react'
import { Platform, StyleSheet, View, TouchableOpacity, TextInput, Image, ActivityIndicator } from 'react-native';
import styles from './styles'
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { ScrollView } from 'react-native-gesture-handler';
import { getCuisinesList } from '../../services/APIService'

let oldSelectedCuisineList = [];
class CuisinesSelection extends Component {

    static navigationOptions = ({ navigation }) => {
        // console.log(`NAV:${JSON.stringify(navigation)}`)
        oldSelectedCuisineList = navigation.getParam('cuisineList', []);
       
        return {
            header: null,
        }
    };


    state = {
        cuisineList: [],
        filteredCuisineList: [],
        isLoading: false,
        selectedCuisineList: [],
        noDataFound: false,
        searchText: ''
    }

    constructor() {
        super()
        //  oldSelectedCuisineList = this.props.navigation.getParam('cuisineList', []);
        console.log(`OLD_LIST:${JSON.stringify(oldSelectedCuisineList)}`)

        //  this.setState({
        //      selectedCuisineList:[...oldSelectedCuisineList]
        //  })
    }

    contains(list, obj) {
        console.log("COMPARE:" + JSON.stringify(list) + " OBJ:" + JSON.stringify(obj))
        var i = list.length;
        while (i--) {
            if (list[i] === obj.name) {
                return true;
            }
        }
        return false;
    }

    componentDidMount() {
        
        this.callGetCuisinesListApi()
    }

    callGetCuisinesListApi = () => {
        if(!this.props.internet){
            alert(strings.message_lno_internet)
            return
        }

        this.setState({
            isLoading: true,
        })

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.cuisineList

        getCuisinesList(url).then(res => {

            console.log("CUISINE_API_RES:" + JSON.stringify(res))

            if (res && res.success && res.data && res.data.length > 0) {

                
                this.setState({
                    noDataFound: false,
                    cuisineList: res.data,
                    filteredCuisineList: res.data,
                    selectedCuisineList: oldSelectedCuisineList,
                    isLoading: false,
                })
            } else {
                this.setState({
                    isLoading: false,
                    noDataFound: true
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
                noDataFound: true
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });
    }

    onDoneClick = () => {
        // console.log("SELECTED:"+JSON.stringify(this.state.selectedCuisineList))
        const { navigation } = this.props;
        navigation.goBack();
        navigation.state.params.onCuisineSelect({
            selectedCuisineList: this.state.selectedCuisineList,
        });
    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }

    handleSearchInput = (e) => {
        this.setState({
            searchText: e
        })

        let text = e.toLowerCase()
        let fullList = this.state.cuisineList;
        let filteredList = fullList.filter((item) => { // search from a full list, and not from a previous search results list
            if (item.name.toLowerCase().match(text))
                return item;
        })
        if (!text || text === '') {
            this.setState({
                filteredCuisineList: filteredList,
                noDataFound: false,
            })
        } else if (!filteredList.length) {
            // set no data flag to true so as to render flatlist conditionally
            this.setState({
                noDataFound: true
            })
        }
        else if (Array.isArray(filteredList)) {
            this.setState({
                noDataFound: false,
                filteredCuisineList: filteredList
            })
        }
    }

    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={styles.activityIndicatorView}>
                    <View style={styles.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={this.state.loading} />
                    </View>
                </View>

            )
        } else {
            return
        }

    }

    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width:25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{ flexDirection: 'row', alignItems: 'center', width: '90%' }}>
                    {this.renderBackButton()}
                    <TextInput style={
                        {
                            width: '90%',
                            paddingLeft: 10,
                            paddingRight: 15,
                            fontFamily: Fonts.Regular,
                            color: Constants.color.fontWhite,
                            fontSize: Platform.OS === Constants.PLATFORM.android ? Constants.fontSize.NormalX : Constants.fontSize.NormalXX
                        }
                    }
                        placeholder={strings.search_cuisines}
                        placeholderTextColor={Constants.color.fontWhite}
                        value={this.state.searchText}
                        autoFocus={false}
                        onChangeText={(text) => this.handleSearchInput(text)}
                    >
                    </TextInput>
                </View>


                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end', width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                        onPress={() => this.setState(prevState => ({
                            noDataFound: false,
                            filteredCuisineList: prevState.cuisineList,
                            searchText: ''
                        }))}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginRight: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }

    itemSelection = (item) => {
        if (this.state.selectedCuisineList.length == 0) {
            this.setState({
                selectedCuisineList: [item.name]
            })
        } else {
            var array = [...this.state.selectedCuisineList]; // make a separate copy of the array
            var index = array.indexOf(item.name)
            if (index !== -1) {
                array.splice(index, 1);
                this.setState({ selectedCuisineList: array });
            } else {
                this.setState(prevState => ({
                    selectedCuisineList: [...prevState.selectedCuisineList, ...[item.name]]
                }))
            }
        }

    }

    renderList() {
        const cuisineList = this.state.filteredCuisineList.map((data) => {
            if (!this.contains(this.state.selectedCuisineList, data)) {
                return (
                    <TouchableOpacity key={data.id} style={[styles.itemTouchable, { backgroundColor: Constants.color.white }]}
                        onPress={() => this.itemSelection(data)}>
                        <TextRegular title={data.name} textStyle={styles.itemText}></TextRegular>
                    </TouchableOpacity>
                )
            } else {
                return (
                    <TouchableOpacity key={data.id} style={[styles.itemTouchable, { backgroundColor: Constants.color.background_for_header }]}
                        onPress={() => this.itemSelection(data)}>
                        <TextRegular title={data.name} textStyle={styles.itemText}></TextRegular>
                        <View style={styles.itemImageView}>
                            <Image source={Images.ic_check_black} style={styles.checkImage}></Image>

                        </View>
                    </TouchableOpacity>
                )
            }

        })

        return (
            <ScrollView style={styles.scrollView}>
                {cuisineList}
            </ScrollView>
        )
    }


    render() {

        return (
            <View style={styles.container}>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                <View style={{ flex: 1 }}>
                    {this.renderToolbar()}
                    <View>
                        {this.renderList()}
                    </View>
                    <View style={styles.doneButtonView} >
                        <TouchableOpacity style={styles.doneButtonTouchable}
                            onPress={() => this.onDoneClick()}>
                            <TextRegular textStyle={styles.doneButtonText} title={strings.done}></TextRegular>
                        </TouchableOpacity>
                    </View>
                </View>

                {this.renderProgressBar()}

                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
            </View>
        );
    };
}

function mapStateToProps(state) {
    return {
        internet:state.internet
    }
}
export default connect(mapStateToProps, {  })(CuisinesSelection)