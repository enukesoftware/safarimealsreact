// @flow
import firebase from 'react-native-firebase';
// Optional flow type
// import RemoteMessage from 'react-native-firebase';

// export default async (message) => {
//     // handle your message

//     return Promise.resolve();
// }


// Optional flow type
import  { RemoteMessage } from 'react-native-firebase';
import { Notification,NotificationOpen} from 'react-native-firebase';

export default async (message) => {
    // handle your message
    // console.log("Message=>",message);
    alert("Message Arrived");
    const notification = new firebase.notifications.Notification()
                      .setNotificationId(message.messageId)
                      .setTitle(message.data.show_name)
                      .setBody(message.data.description)
                      .setData(message.data)
                      .android.setChannelId('podpitara_app')
                      .android.setBigPicture(message.data.showImage)
                      .android.setPriority(firebase.notifications.Android.Priority.High);
    firebase.notifications().displayNotification(notification).catch(err => alert("Error in Background"));
    return Promise.resolve();
}